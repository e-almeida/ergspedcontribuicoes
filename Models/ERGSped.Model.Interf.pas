unit ERGSped.Model.Interf;

interface

uses SPEDFacade.Model.Interf, ConexaoFacade.Model.Interf,
  ConfiguracoesFactory.Model.Interf, EntidadeFacade.Model.Interf;

type
  IERGSpedModel = interface
    ['{8EE2B5B9-9E95-4898-BEB7-EF945600B932}']
    function SPED: ISPEDFacadeModel;
    function Conexao: IConexaoFacadeModel;
    function Entidade: IEntidadeFacadeModel;

    function Configuracoes: IConfiguracoesFactoryModel;
  end;

implementation

end.
