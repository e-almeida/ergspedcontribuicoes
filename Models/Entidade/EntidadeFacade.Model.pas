unit EntidadeFacade.Model;

interface

uses EntidadeFacade.Model.Interf,
  SPEDContribuicoesEntidadeFactory.Model.Interf;

type
  TEntidadeFacadeModel = class(TInterfacedObject, IEntidadeFacadeModel)
  private

  public
    constructor create;
    destructor Destroy; override;

    class function New: IEntidadeFacadeModel;

    function SPEDContribuicoesFactory: ISPEDContribuicoesEntidadeFactoryModel;
  end;

implementation

{ TEntidadeFacadeModel }

uses SPEDContribuicoesEntidadeFactory.Model;

constructor TEntidadeFacadeModel.create;
begin

end;

destructor TEntidadeFacadeModel.Destroy;
begin

  inherited;
end;

class function TEntidadeFacadeModel.New: IEntidadeFacadeModel;
begin
  Result := Self.create
end;

function TEntidadeFacadeModel.SPEDContribuicoesFactory
  : ISPEDContribuicoesEntidadeFactoryModel;
begin
  Result := TSPEDContribuicoesEntidadeFactoryModel.New;
end;

end.
