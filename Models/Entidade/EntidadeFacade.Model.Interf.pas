unit EntidadeFacade.Model.Interf;

interface

uses SPEDContribuicoesEntidadeFactory.Model.Interf;

type
  IEntidadeFacadeModel = interface
    ['{DB623001-9D25-4389-BC2D-7E8BD32ED231}']
    function SPEDContribuicoesFactory: ISPEDContribuicoesEntidadeFactoryModel;
  end;

implementation

end.
