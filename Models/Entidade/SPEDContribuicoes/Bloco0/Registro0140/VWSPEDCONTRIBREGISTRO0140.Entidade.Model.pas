unit VWSPEDCONTRIBREGISTRO0140.Entidade.Model;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('VWSPEDCONTRIBREGISTRO0140', '')]
  TVWSPEDCONTRIBREGISTRO0140 = class
  private
    { Private declarations } 
    FCODIGO: Nullable<Integer>;
    FNOME: Nullable<String>;
    FCNPJ: Nullable<String>;
    FUF: Nullable<String>;
    FIE: Nullable<String>;
    FCOD_MUN: Nullable<Integer>;
    FIM: Nullable<String>;
    FSUFRAMA: Nullable<String>;
  public 
    { Public declarations } 
    [Column('CODIGO', ftInteger)]
    [Dictionary('CODIGO', 'Mensagem de valida��o', '', '', '', taCenter)]
    property CODIGO: Nullable<Integer> read FCODIGO write FCODIGO;

    [Column('NOME', ftString, 60)]
    [Dictionary('NOME', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property NOME: Nullable<String> read FNOME write FNOME;

    [Column('CNPJ', ftString, 20)]
    [Dictionary('CNPJ', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property CNPJ: Nullable<String> read FCNPJ write FCNPJ;

    [Column('UF', ftString, 2)]
    [Dictionary('UF', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property UF: Nullable<String> read FUF write FUF;

    [Column('IE', ftString, 20)]
    [Dictionary('IE', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property IE: Nullable<String> read FIE write FIE;

    [Column('COD_MUN', ftInteger)]
    [Dictionary('COD_MUN', 'Mensagem de valida��o', '', '', '', taCenter)]
    property COD_MUN: Nullable<Integer> read FCOD_MUN write FCOD_MUN;

    [Column('IM', ftString, 20)]
    [Dictionary('IM', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property IM: Nullable<String> read FIM write FIM;

    [Column('SUFRAMA', ftString)]
    [Dictionary('SUFRAMA', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property SUFRAMA: Nullable<String> read FSUFRAMA write FSUFRAMA;
  end;

implementation

initialization

  TRegisterClass.RegisterEntity(TVWSPEDCONTRIBREGISTRO0140)

end.
