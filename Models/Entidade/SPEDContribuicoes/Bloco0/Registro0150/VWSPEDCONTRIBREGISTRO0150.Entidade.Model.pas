unit VWSPEDCONTRIBREGISTRO0150.Entidade.Model;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping,
  ormbr.types.nullable,
  ormbr.mapping.classes,
  ormbr.mapping.register,
  ormbr.mapping.attributes;

type
  [Entity]
  [Table('VWSPEDCONTRIBREGISTRO0150', '')]
  TVWSPEDCONTRIBREGISTRO0150 = class
  private
    { Private declarations }
    FEMPRESA: Integer;
    FTIPO: String;
    FCOD_PART: string;
    FNOME: String;
    FCOD_PAIS: Integer;
    FCPFCNPJ: String;
    FRGIE: String;
    FCOD_MUN: Integer;
    FSUFRAMA: String;
    FENDERECO: String;
    FNUM: String;
    FCOMPL: String;
    FBAIRRO: String;
  public
    { Public declarations }
    [Column('EMPRESA', ftInteger)]
    [Dictionary('EMPRESA', 'Mensagem de valida��o', '', '', '', taCenter)]
    property EMPRESA: Integer read FEMPRESA write FEMPRESA;

    [Column('TIPO', ftString, 40)]
    [Dictionary('TIPO', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property TIPO: String read FTIPO write FTIPO;

    [Column('COD_PART', ftString)]
    [Dictionary('COD_PART', 'Mensagem de valida��o', '', '', '', taCenter)]
    property COD_PART: string read FCOD_PART write FCOD_PART;

    [Column('NOME', ftString, 60)]
    [Dictionary('NOME', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property NOME: String read FNOME write FNOME;

    [Column('COD_PAIS', ftInteger)]
    [Dictionary('COD_PAIS', 'Mensagem de valida��o', '', '', '', taCenter)]
    property COD_PAIS: Integer read FCOD_PAIS write FCOD_PAIS;

    [Column('CPFCNPJ', ftString, 18)]
    [Dictionary('CPFCNPJ', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property CPFCNPJ: String read FCPFCNPJ write FCPFCNPJ;

    [Column('RGIE', ftString, 20)]
    [Dictionary('RGIE', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property RGIE: String read FRGIE write FRGIE;

    [Column('COD_MUN', ftInteger)]
    [Dictionary('COD_MUN', 'Mensagem de valida��o', '', '', '', taCenter)]
    property COD_MUN: Integer read FCOD_MUN write FCOD_MUN;

    [Column('SUFRAMA', ftString, 1)]
    [Dictionary('SUFRAMA', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property SUFRAMA: String read FSUFRAMA write FSUFRAMA;

    [Column('ENDERECO', ftString, 40)]
    [Dictionary('ENDERECO', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property ENDERECO: String read FENDERECO write FENDERECO;

    [Column('NUM', ftString, 10)]
    [Dictionary('NUM', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property NUM: String read FNUM write FNUM;

    [Column('COMPL', ftString, 255)]
    [Dictionary('COMPL', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property COMPL: String read FCOMPL write FCOMPL;

    [Column('BAIRRO', ftString, 20)]
    [Dictionary('BAIRRO', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property BAIRRO: String read FBAIRRO write FBAIRRO;
  end;

implementation

initialization

  TRegisterClass.RegisterEntity(TVWSPEDCONTRIBREGISTRO0150)

end.
