unit VWSPEDCONTRIBREGISTRO0100.Entidade.Model;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('VWSPEDCONTRIBREGISTRO0100', '')]
  TVWSPEDCONTRIBREGISTRO0100 = class
  private
    { Private declarations } 
    FCODIGO: Nullable<Integer>;
    FNOME: Nullable<String>;
    FCPF: Nullable<String>;
    FCRC: Nullable<String>;
    FCNPJ: String;
    FCEP: Nullable<String>;
    FENDERECO: Nullable<String>;
    FNUM: Nullable<String>;
    FCOMPL: String;
    FBAIRRO: Nullable<String>;
    FFONE: Nullable<String>;
    FFAX: String;
    FEMAIL: Nullable<String>;
    FCOD_MUN: Nullable<Integer>;
  public 
    { Public declarations } 
    [Column('CODIGO', ftInteger)]
    [Dictionary('CODIGO', 'Mensagem de valida��o', '', '', '', taCenter)]
    property CODIGO: Nullable<Integer> read FCODIGO write FCODIGO;

    [Column('NOME', ftString, 60)]
    [Dictionary('NOME', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property NOME: Nullable<String> read FNOME write FNOME;

    [Column('CPF', ftString, 11)]
    [Dictionary('CPF', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property CPF: Nullable<String> read FCPF write FCPF;

    [Column('CRC', ftString, 11)]
    [Dictionary('CRC', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property CRC: Nullable<String> read FCRC write FCRC;

    [Column('CNPJ', ftString, 14)]
    [Dictionary('CNPJ', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property CNPJ: String read FCNPJ write FCNPJ;

    [Column('CEP', ftString, 8)]
    [Dictionary('CEP', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property CEP: Nullable<String> read FCEP write FCEP;

    [Column('ENDERECO', ftString, 60)]
    [Dictionary('ENDERECO', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property ENDERECO: Nullable<String> read FENDERECO write FENDERECO;

    [Column('NUM', ftString, 10)]
    [Dictionary('NUM', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property NUM: Nullable<String> read FNUM write FNUM;

    [Column('COMPL', ftString, 60)]
    [Dictionary('COMPL', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property COMPL: String read FCOMPL write FCOMPL;

    [Column('BAIRRO', ftString, 60)]
    [Dictionary('BAIRRO', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property BAIRRO: Nullable<String> read FBAIRRO write FBAIRRO;

    [Column('FONE', ftString, 11)]
    [Dictionary('FONE', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property FONE: Nullable<String> read FFONE write FFONE;

    [Column('FAX', ftString, 11)]
    [Dictionary('FAX', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property FAX: String read FFAX write FFAX;

    [Column('EMAIL', ftString, 100)]
    [Dictionary('EMAIL', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property EMAIL: Nullable<String> read FEMAIL write FEMAIL;

    [Column('COD_MUN', ftInteger)]
    [Dictionary('COD_MUN', 'Mensagem de valida��o', '', '', '', taCenter)]
    property COD_MUN: Nullable<Integer> read FCOD_MUN write FCOD_MUN;
  end;

implementation

initialization

  TRegisterClass.RegisterEntity(TVWSPEDCONTRIBREGISTRO0100)

end.
