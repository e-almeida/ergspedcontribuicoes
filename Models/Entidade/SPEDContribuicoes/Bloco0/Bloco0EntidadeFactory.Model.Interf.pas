unit Bloco0EntidadeFactory.Model.Interf;

interface

uses
  VWSPEDCONTRIBREGISTRO0000.Entidade.Model,
  VWSPEDCONTRIBREGISTRO0100.Entidade.Model,
  VWSPEDCONTRIBREGISTRO0140.Entidade.Model,
  VWSPEDCONTRIBREGISTRO0200.Entidade.Model;

type
  IBloco0EntidadeFactoryModel = interface
    ['{1115BE19-0EED-4163-952E-197D99E17118}']
    function Registro0000: TVWSPEDCONTRIBREGISTRO0000;
    function Registro0100: TVWSPEDCONTRIBREGISTRO0100;
    function Registro0140: TVWSPEDCONTRIBREGISTRO0140;
    function Registro0200: TVWSPEDCONTRIBREGISTRO0200;
  end;

implementation

end.
