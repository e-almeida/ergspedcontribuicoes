unit Bloco0EntidadeFactory.Model;

interface

uses
  Bloco0EntidadeFactory.Model.Interf,
  VWSPEDCONTRIBREGISTRO0000.Entidade.Model,
  VWSPEDCONTRIBREGISTRO0100.Entidade.Model,
  VWSPEDCONTRIBREGISTRO0140.Entidade.Model,
  VWSPEDCONTRIBREGISTRO0200.Entidade.Model;

type
  TBloco0EntidadeFactoryModel = class(TInterfacedObject,
    IBloco0EntidadeFactoryModel)
  private

  public
    constructor create;
    destructor Destroy; override;

    class function New: IBloco0EntidadeFactoryModel;

    function Registro0000: TVWSPEDCONTRIBREGISTRO0000;
    function Registro0100: TVWSPEDCONTRIBREGISTRO0100;
    function Registro0140: TVWSPEDCONTRIBREGISTRO0140;
    function Registro0200: TVWSPEDCONTRIBREGISTRO0200;
  end;

implementation

{ TBloco0EntidadeFactoryModel }

constructor TBloco0EntidadeFactoryModel.create;
begin

end;

destructor TBloco0EntidadeFactoryModel.Destroy;
begin

  inherited;
end;

class function TBloco0EntidadeFactoryModel.New: IBloco0EntidadeFactoryModel;
begin
  Result := Self.create
end;

function TBloco0EntidadeFactoryModel.Registro0000: TVWSPEDCONTRIBREGISTRO0000;
begin
  Result := TVWSPEDCONTRIBREGISTRO0000.create;
end;

function TBloco0EntidadeFactoryModel.Registro0100: TVWSPEDCONTRIBREGISTRO0100;
begin
  Result := TVWSPEDCONTRIBREGISTRO0100.create;
end;

function TBloco0EntidadeFactoryModel.Registro0140: TVWSPEDCONTRIBREGISTRO0140;
begin
  Result := TVWSPEDCONTRIBREGISTRO0140.create;
end;

function TBloco0EntidadeFactoryModel.Registro0200: TVWSPEDCONTRIBREGISTRO0200;
begin
  Result := TVWSPEDCONTRIBREGISTRO0200.create;
end;

end.
