unit VWSPEDCONTRIBREGISTRO0000.Entidade.Model;

interface

uses
  DB,
  Classes,
  SysUtils,
  Generics.Collections,

  /// orm
  ormbr.types.blob,
  ormbr.types.lazy,
  ormbr.types.mapping,
  ormbr.types.nullable,
  ormbr.mapping.classes,
  ormbr.mapping.register,
  ormbr.mapping.attributes;

type
  [Entity]
  [Table('VWSPEDCONTRIBREGISTRO0000', '')]
  TVWSPEDCONTRIBREGISTRO0000 = class
  private
    { Private declarations }
    FCODIGO: Nullable<Integer>;
    FNOME: Nullable<String>;
    FCNPJ: Nullable<String>;
    FUF: Nullable<String>;
    FCOD_MUN: Nullable<Integer>;
    FSUFRAMA: Nullable<String>;
  public
    { Public declarations }
    [Column('CODIGO', ftInteger)]
    [Dictionary('CODIGO', 'Mensagem de valida��o', '', '', '', taCenter)]
    property CODIGO: Nullable<Integer> read FCODIGO write FCODIGO;

    [Column('NOME', ftString, 60)]
    [Dictionary('NOME', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property NOME: Nullable<String> read FNOME write FNOME;

    [Column('CNPJ', ftString, 20)]
    [Dictionary('CNPJ', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property CNPJ: Nullable<String> read FCNPJ write FCNPJ;

    [Column('UF', ftString, 2)]
    [Dictionary('UF', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property UF: Nullable<String> read FUF write FUF;

    [Column('COD_MUN', ftInteger)]
    [Dictionary('COD_MUN', 'Mensagem de valida��o', '', '', '', taCenter)]
    property COD_MUN: Nullable<Integer> read FCOD_MUN write FCOD_MUN;

    [Column('SUFRAMA', ftString)]
    [Dictionary('SUFRAMA', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property SUFRAMA: Nullable<String> read FSUFRAMA write FSUFRAMA;
  end;

implementation

initialization

  TRegisterClass.RegisterEntity(TVWSPEDCONTRIBREGISTRO0000)

end.
