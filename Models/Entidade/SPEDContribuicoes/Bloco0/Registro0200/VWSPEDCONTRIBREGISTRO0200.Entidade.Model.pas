unit VWSPEDCONTRIBREGISTRO0200.Entidade.Model;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('VWSPEDCONTRIBREGISTRO0200', '')]
  TVWSPEDCONTRIBREGISTRO0200 = class
  private
    { Private declarations } 
    FEMPRESA: Integer;
    FCOD_ITEM: String;
    FDESCRICAO: String;
    FCOD_BARRA: String;
    FUNM: String;
    FCOD_NCM: String;
    FTIPO: String;
    FEX_IPI: String;
    FCOD_GEN: String;
    FCOD_LST: String;
    FALIQ_ICMS: Double;
  public
    { Public declarations }
    [Column('EMPRESA', ftInteger)]
    [Dictionary('EMPRESA', 'Mensagem de valida��o', '', '', '', taCenter)]
    property EMPRESA: Integer read FEMPRESA write FEMPRESA;

    [Column('COD_ITEM', ftString, 10)]
    [Dictionary('COD_ITEM', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property COD_ITEM: String read FCOD_ITEM write FCOD_ITEM;

    [Column('DESCRICAO', ftString, 205)]
    [Dictionary('DESCRICAO', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property DESCRICAO: String read FDESCRICAO write FDESCRICAO;

    [Column('COD_BARRA', ftString, 16)]
    [Dictionary('COD_BARRA', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property COD_BARRA: String read FCOD_BARRA write FCOD_BARRA;

    [Column('UNM', ftString, 2)]
    [Dictionary('UNM', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property UNM: String read FUNM write FUNM;

    [Column('COD_NCM', ftString, 20)]
    [Dictionary('COD_NCM', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property COD_NCM: String read FCOD_NCM write FCOD_NCM;

    [Column('TIPO', ftString, 2)]
    [Dictionary('TIPO', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property TIPO: String read FTIPO write FTIPO;

    [Column('EX_IPI', ftString, 22)]
    [Dictionary('EX_IPI', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property EX_IPI: String read FEX_IPI write FEX_IPI;

    [Column('COD_GEN', ftString, 22)]
    [Dictionary('COD_GEN', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property COD_GEN: String read FCOD_GEN write FCOD_GEN;

    [Column('COD_LST', ftString, 22)]
    [Dictionary('COD_LST', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property COD_LST: String read FCOD_LST write FCOD_LST;

    [Column('ALIQ_ICMS', ftBCD, 18, 2)]
    [Dictionary('ALIQ_ICMS', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property ALIQ_ICMS: Double read FALIQ_ICMS write FALIQ_ICMS;
  end;

implementation

initialization

  TRegisterClass.RegisterEntity(TVWSPEDCONTRIBREGISTRO0200)

end.
