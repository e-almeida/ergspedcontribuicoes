unit VWSPEDCONTRIBREGISTRO0190.Entidade.Model;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('VWSPEDCONTRIBREGISTRO0190', '')]
  TVWSPEDCONTRIBREGISTRO0190 = class
  private
    { Private declarations } 
    FEMPRESA: Nullable<Integer>;
    FUNM: Nullable<String>;
    FDESCRICAO: Nullable<String>;
  public 
    { Public declarations } 
    [Column('EMPRESA', ftInteger)]
    [Dictionary('EMPRESA', 'Mensagem de valida��o', '', '', '', taCenter)]
    property EMPRESA: Nullable<Integer> read FEMPRESA write FEMPRESA;

    [Column('UNM', ftString, 2)]
    [Dictionary('UNM', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property UNM: Nullable<String> read FUNM write FUNM;

    [Column('DESCRICAO', ftString, 55)]
    [Dictionary('DESCRICAO', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property DESCRICAO: Nullable<String> read FDESCRICAO write FDESCRICAO;
  end;

implementation

initialization

  TRegisterClass.RegisterEntity(TVWSPEDCONTRIBREGISTRO0190)

end.
