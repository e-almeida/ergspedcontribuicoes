unit SPEDContribuicoesEntidadeFactory.Model;

interface

uses SPEDContribuicoesEntidadeFactory.Model.Interf,
  Bloco0EntidadeFactory.Model.Interf, BlocoAEntidadeFactory.Model.Interf;

type
  TSPEDContribuicoesEntidadeFactoryModel = class(TInterfacedObject,
    ISPEDContribuicoesEntidadeFactoryModel)
  private

  public
    constructor create;
    destructor Destroy; override;

    class function New: ISPEDContribuicoesEntidadeFactoryModel;

    function Bloco0: IBloco0EntidadeFactoryModel;
    function BlocoA: IBlocoAEntidadeFactoryModel;

  end;

implementation

{ TSPEDContribuicoesEntidadeFactoryModel }

uses Bloco0EntidadeFactory.Model, BlocoAEntidadeFactory.Model;

function TSPEDContribuicoesEntidadeFactoryModel.Bloco0
  : IBloco0EntidadeFactoryModel;
begin
  Result := TBloco0EntidadeFactoryModel.New;
end;

function TSPEDContribuicoesEntidadeFactoryModel.BlocoA
  : IBlocoAEntidadeFactoryModel;
begin
  Result := TBlocoAEntidadeFactoryModel.New;
end;

constructor TSPEDContribuicoesEntidadeFactoryModel.create;
begin

end;

destructor TSPEDContribuicoesEntidadeFactoryModel.Destroy;
begin

  inherited;
end;

class function TSPEDContribuicoesEntidadeFactoryModel.New
  : ISPEDContribuicoesEntidadeFactoryModel;
begin
  Result := Self.create
end;

end.
