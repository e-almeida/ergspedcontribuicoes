unit VWSPEDCONTRIBREGISTROA100.Entidade.Model;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('VWSPEDCONTRIBREGISTROA100', '')]
  TVWSPEDCONTRIBREGISTROA100 = class
  private
    { Private declarations } 
    FEMPRESA: Integer;
    FCOD_PART: String;
    FNUM_DOC: String;
    FCOD_SIT: Integer;
    FDT_DOC: TDateTime;
    FDT_EXE_SERV: TDateTime;
    FVL_DOC: Double;
    FIND_PGTO: Integer;
    FVL_DESC: Double;
    FVL_BC_PIS: Double;
    FVL_PIS: Double;
    FVL_BC_COFINS: Double;
    FVL_COFINS: Double;
    FVL_PIS_RET: Double;
    FVL_COFINS_RET: Double;
    FVL_ISS: Double;
  public 
    { Public declarations } 
    [Column('EMPRESA', ftInteger)]
    [Dictionary('EMPRESA', 'Mensagem de valida��o', '', '', '', taCenter)]
    property EMPRESA: Integer read FEMPRESA write FEMPRESA;

    [Column('COD_PART', ftString, 7)]
    [Dictionary('COD_PART', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property COD_PART: String read FCOD_PART write FCOD_PART;

    [Column('NUM_DOC', ftString, 8)]
    [Dictionary('NUM_DOC', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property NUM_DOC: String read FNUM_DOC write FNUM_DOC;

    [Column('COD_SIT', ftInteger)]
    [Dictionary('COD_SIT', 'Mensagem de valida��o', '', '', '', taCenter)]
    property COD_SIT: Integer read FCOD_SIT write FCOD_SIT;

    [Column('DT_DOC', ftDateTime)]
    [Dictionary('DT_DOC', 'Mensagem de valida��o', '', '', '', taCenter)]
    property DT_DOC: TDateTime read FDT_DOC write FDT_DOC;

    [Column('DT_EXE_SERV', ftDateTime)]
    [Dictionary('DT_EXE_SERV', 'Mensagem de valida��o', '', '', '', taCenter)]
    property DT_EXE_SERV: TDateTime read FDT_EXE_SERV write FDT_EXE_SERV;

    [Column('VL_DOC', ftBCD, 18, 4)]
    [Dictionary('VL_DOC', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property VL_DOC: Double read FVL_DOC write FVL_DOC;

    [Column('IND_PGTO', ftInteger)]
    [Dictionary('IND_PGTO', 'Mensagem de valida��o', '', '', '', taCenter)]
    property IND_PGTO: Integer read FIND_PGTO write FIND_PGTO;

    [Column('VL_DESC', ftBCD, 18, 4)]
    [Dictionary('VL_DESC', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property VL_DESC: Double read FVL_DESC write FVL_DESC;

    [Column('VL_BC_PIS', ftBCD, 18, 4)]
    [Dictionary('VL_BC_PIS', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property VL_BC_PIS: Double read FVL_BC_PIS write FVL_BC_PIS;

    [Column('VL_PIS', ftBCD, 18, 4)]
    [Dictionary('VL_PIS', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property VL_PIS: Double read FVL_PIS write FVL_PIS;

    [Column('VL_BC_COFINS', ftBCD, 18, 4)]
    [Dictionary('VL_BC_COFINS', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property VL_BC_COFINS: Double read FVL_BC_COFINS write FVL_BC_COFINS;

    [Column('VL_COFINS', ftBCD, 18, 4)]
    [Dictionary('VL_COFINS', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property VL_COFINS: Double read FVL_COFINS write FVL_COFINS;

    [Column('VL_PIS_RET', ftBCD, 18, 2)]
    [Dictionary('VL_PIS_RET', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property VL_PIS_RET: Double read FVL_PIS_RET write FVL_PIS_RET;

    [Column('VL_COFINS_RET', ftBCD, 18, 2)]
    [Dictionary('VL_COFINS_RET', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property VL_COFINS_RET: Double read FVL_COFINS_RET write FVL_COFINS_RET;

    [Column('VL_ISS', ftBCD, 18, 4)]
    [Dictionary('VL_ISS', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property VL_ISS: Double read FVL_ISS write FVL_ISS;
  end;

implementation

initialization

  TRegisterClass.RegisterEntity(TVWSPEDCONTRIBREGISTROA100)

end.
