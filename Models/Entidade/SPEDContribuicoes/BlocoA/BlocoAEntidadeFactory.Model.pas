unit BlocoAEntidadeFactory.Model;

interface

uses BlocoAEntidadeFactory.Model.Interf,
  VWSPEDCONTRIBREGISTROA100.Entidade.Model;

type
  TBlocoAEntidadeFactoryModel = class(TInterfacedObject,
    IBlocoAEntidadeFactoryModel)
  private

  public
    constructor create;
    destructor Destroy; override;

    class function New: IBlocoAEntidadeFactoryModel;

    function RegistroA100: TVWSPEDCONTRIBREGISTROA100;
  end;

implementation

{ TBlocoAEntidadeFactoryModel }

constructor TBlocoAEntidadeFactoryModel.create;
begin

end;

destructor TBlocoAEntidadeFactoryModel.Destroy;
begin

  inherited;
end;

class function TBlocoAEntidadeFactoryModel.New: IBlocoAEntidadeFactoryModel;
begin
  Result := Self.create
end;

function TBlocoAEntidadeFactoryModel.RegistroA100: TVWSPEDCONTRIBREGISTROA100;
begin
  Result := TVWSPEDCONTRIBREGISTROA100.create;
end;

end.
