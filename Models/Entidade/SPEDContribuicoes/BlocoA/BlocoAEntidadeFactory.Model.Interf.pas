unit BlocoAEntidadeFactory.Model.Interf;

interface

uses VWSPEDCONTRIBREGISTROA100.Entidade.Model;

type
  IBlocoAEntidadeFactoryModel = interface
    ['{39645815-2C5E-43FD-ADB6-0E33727F9F54}']
    function RegistroA100: TVWSPEDCONTRIBREGISTROA100;
  end;

implementation

end.
