unit SPEDContribuicoesEntidadeFactory.Model.Interf;

interface

uses Bloco0EntidadeFactory.Model.Interf, BlocoAEntidadeFactory.Model.Interf;

type
  ISPEDContribuicoesEntidadeFactoryModel = interface
    ['{A2D93390-E31C-420D-A4D1-DD0B70E17928}']
    function Bloco0: IBloco0EntidadeFactoryModel;
    function BlocoA: IBlocoAEntidadeFactoryModel;
  end;

implementation

end.
