unit Conexao.Model.Interf;

interface

uses
  Data.DB,

  ormbr.Factory.Interfaces;

type
  IConexaoModel = interface
    ['{849B3BAE-6A1C-4DF9-B274-A29FF3B25EE2}']
    function Conexao : IDBConnection;
  end;

implementation

end.
