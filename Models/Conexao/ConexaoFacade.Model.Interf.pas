unit ConexaoFacade.Model.Interf;

interface

uses FireDACFactory.Model.Interf;

type
  IConexaoFacadeModel = interface
    ['{5A186D7F-C3F1-4037-9B80-E024DA5AE976}']
    function FireDAC: IFireDACFactoryModel;
  end;

implementation

end.
