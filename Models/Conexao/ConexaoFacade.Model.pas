unit ConexaoFacade.Model;

interface

uses ConexaoFacade.Model.Interf, FireDACFactory.Model.Interf;

type
  TConexaoFacadeModel = class(TInterfacedObject, IConexaoFacadeModel)
  private

  public
    constructor Create;
    destructor Destroy; override;

    class function New: IConexaoFacadeModel;

    function FireDAC: IFireDACFactoryModel;
  end;

implementation

{ TConexaoFacadeModel }

uses FireDACFactory.Model;

constructor TConexaoFacadeModel.Create;
begin

end;

destructor TConexaoFacadeModel.Destroy;
begin

  inherited;
end;

function TConexaoFacadeModel.FireDAC: IFireDACFactoryModel;
begin
  Result := TFireDACFactoryModel.New;
end;

class function TConexaoFacadeModel.New: IConexaoFacadeModel;
begin
  Result := Self.Create;
end;

end.
