unit FiredacFirebird.Model;

interface

uses
  System.SysUtils,
  System.Classes,
  Data.DB,

  FireDAC.Stan.Intf,
  FireDAC.Stan.Option,
  FireDAC.Stan.Error,
  FireDAC.UI.Intf,
  FireDAC.Phys.Intf,
  FireDAC.Stan.Def,
  FireDAC.Stan.Pool,
  FireDAC.Stan.Async,
  FireDAC.Phys,
  FireDAC.VCLUI.Wait,
  FireDAC.Comp.Client,
  FireDAC.Comp.UI,
  FireDAC.Phys.FBDef,
  FireDAC.Phys.IBBase,
  FireDAC.Phys.FB,

  ormbr.Factory.Interfaces,
  ormbr.Factory.FireDAC,
  ormbr.dml.Generator.Firebird,

  Conexao.Model.Interf, ConfiguracoesConexao.Model.Interf;

type
  TFiredacFirebirdModel = class(TDataModule, IConexaoModel)
    FDBancoDados: TFDConnection;
    FDCursor: TFDGUIxWaitCursor;
    FDDriver: TFDPhysFBDriverLink;
    procedure DataModuleCreate(Sender: TObject);
  private
    FConexao: IDBConnection;
    FConfiguracoes: IConfiguracoesConexaoModel;
    procedure ConectaNoBancoDeDados;
  public
    class function New: IConexaoModel;

    function Conexao : IDBConnection;
  end;

var
  FiredacFirebirdModel: TFiredacFirebirdModel;

implementation

uses
  Vcl.Forms, ERGSped.Model;

{ %CLASSGROUP 'Vcl.Controls.TControl' }

{$R *.dfm}
{ TConexaoFirebirdModel }

procedure TFiredacFirebirdModel.ConectaNoBancoDeDados;
begin
  FConfiguracoes := TERGSPEDModel.New.Configuracoes.Conexao.PrepararConfiguracoes;

  try
     FDBancoDados.Connected             := False;
     FDBancoDados.DriverName            := FConfiguracoes.Driver;
     FDBancoDados.LoginPrompt           := True;
     FDBancoDados.Params.Database       := FConfiguracoes.BancoDeDados;
     FDBancoDados.Params.Add('Server='  +  FConfiguracoes.Servidor);
     FDBancoDados.Params.Add('Port='    +  FConfiguracoes.Porta);
     FDBancoDados.Params.Add('UserName='+  FConfiguracoes.Usuario);
     FDBancoDados.Params.Add('PassWord='+  FConfiguracoes.Senha);
     FDBancoDados.Connected             := True;
     FDDriver.VendorLib                 := FConfiguracoes.Biblioteca;

  except on E: Exception do
     raise Exception.Create(e.Message);
  end;
end;

function TFiredacFirebirdModel.Conexao: IDBConnection;
begin
  Result := FConexao;
end;

procedure TFiredacFirebirdModel.DataModuleCreate(Sender: TObject);
begin
  ConectaNoBancoDeDados;

  FConexao := TFactoryFireDAC.Create(FDBancoDados, dnFirebird);
end;

class function TFiredacFirebirdModel.New: IConexaoModel;
begin
  Result := Self.Create(nil);
end;

end.
