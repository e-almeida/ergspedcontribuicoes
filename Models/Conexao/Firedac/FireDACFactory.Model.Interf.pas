unit FireDACFactory.Model.Interf;

interface

uses Conexao.Model.Interf;

type

  IFireDACFactoryModel = interface
    ['{9D51DE4B-3111-49D5-9E82-D5110FC2B63F}']
    function Firebird: IConexaoModel;
  end;

implementation

end.
