unit FireDACFactory.Model;

interface

uses FireDACFactory.Model.Interf, Conexao.Model.Interf;

type
  TFireDACFactoryModel = class(TInterfacedObject, IFireDACFactoryModel)
  private

  public
    constructor Create;
    destructor Destroy; override;

    class function New: IFireDACFactoryModel;

    function Firebird: IConexaoModel;
  end;

implementation

{ TFireDACFactoryModel }

uses FiredacFirebird.Model;

constructor TFireDACFactoryModel.Create;
begin

end;

destructor TFireDACFactoryModel.Destroy;
begin

  inherited;
end;

function TFireDACFactoryModel.Firebird: IConexaoModel;
begin
 Result :=  TFiredacFirebirdModel.New;
end;

class function TFireDACFactoryModel.New: IFireDACFactoryModel;
begin
  Result := Self.Create;
end;

end.
