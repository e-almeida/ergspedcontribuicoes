unit ERGSped.Model;

interface

uses ERGSped.Model.Interf, SPEDFacade.Model.Interf, ConexaoFacade.Model.Interf,
  ConfiguracoesFactory.Model.Interf, EntidadeFacade.Model.Interf;

type
  TERGSPEDModel = class(TInterfacedObject, IERGSpedModel)
  private

  public
    constructor create;
    destructor Destroy; override;

    class function New: IERGSpedModel;

    function SPED: ISPEDFacadeModel;
    function Conexao: IConexaoFacadeModel;
    function Entidade: IEntidadeFacadeModel;

    function Configuracoes: IConfiguracoesFactoryModel;

  end;

implementation

{ TERGSPEDModel }

uses SPEDFacade.Model, ConexaoFacade.Model, ConfiguracoesFactory.Model,
  EntidadeFacade.Model;

function TERGSPEDModel.Conexao: IConexaoFacadeModel;
begin
  Result := TConexaoFacadeModel.New;
end;

function TERGSPEDModel.Configuracoes: IConfiguracoesFactoryModel;
begin
  Result := TConfiguracoesFactoryModel.New;
end;

constructor TERGSPEDModel.create;
begin

end;

destructor TERGSPEDModel.Destroy;
begin

  inherited;
end;

function TERGSPEDModel.Entidade: IEntidadeFacadeModel;
begin
  Result := TEntidadeFacadeModel.New;
end;

class function TERGSPEDModel.New: IERGSpedModel;
begin
  Result := Self.create
end;

function TERGSPEDModel.SPED: ISPEDFacadeModel;
begin
  Result := TSPEDFacadeModel.New;
end;

end.
