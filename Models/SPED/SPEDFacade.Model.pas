unit SPEDFacade.Model;

interface

uses SPEDFacade.Model.Interf, SPEDContribuicoesFactory.Model.Interf;

type
  TSPEDFacadeModel = class(TInterfacedObject, ISPEDFacadeModel)
  private

  public
    constructor create;
    destructor Destroy; override;

    class function New: ISPEDFacadeModel;

    function Contribuicoes: ISPEDContribuicoesFactoryModel;
  end;

implementation

{ TSPEDFacadeModel }

uses SPEDContribuicoesFactory.Model;

function TSPEDFacadeModel.Contribuicoes: ISPEDContribuicoesFactoryModel;
begin
  Result := TSPEDContribuicoesFactoryModel.New;
end;

constructor TSPEDFacadeModel.create;
begin

end;

destructor TSPEDFacadeModel.Destroy;
begin

  inherited;
end;

class function TSPEDFacadeModel.New: ISPEDFacadeModel;
begin
  Result := Self.create
end;

end.
