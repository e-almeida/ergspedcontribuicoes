unit SPEDContribuicoesFactory.Model.Interf;

interface

uses SPEDContribuicoesBloco0.Model.Interf, SPEDContribuicoesBlocoA.Model.Interf;

type
 ISPEDContribuicoesFactoryModel = interface
   ['{216A1F2F-EBC5-4757-BF51-8DE363B217EC}']
   function Bloco0: ISPEDContribuicoesBloco0Model;
   function BlocoA: ISPEDContribuicoesBlocoAModel;
 end;

implementation

end.
