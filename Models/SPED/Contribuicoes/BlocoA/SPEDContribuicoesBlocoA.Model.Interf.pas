unit SPEDContribuicoesBlocoA.Model.Interf;

interface

uses
  ACBrEPCBloco_A_Class;

type
  ISPEDContribuicoesBlocoAModel = Interface
    ['{79619A03-B2BE-4A85-84AC-C7C15515A514}']
    function Empresa(AValue: Integer): ISPEDContribuicoesBlocoAModel;
    function DataInicial(AValue: TDate): ISPEDContribuicoesBlocoAModel;
    function DataFinal(AValue: TDate): ISPEDContribuicoesBlocoAModel;
    Function RegistroA001: ISPEDContribuicoesBlocoAModel;
    Function RegistroA010: ISPEDContribuicoesBlocoAModel;
    Function RegistroA100: ISPEDContribuicoesBlocoAModel;
    Function RegistroA170: ISPEDContribuicoesBlocoAModel;
    function &End: TBloco_A;
  End;

implementation

end.
