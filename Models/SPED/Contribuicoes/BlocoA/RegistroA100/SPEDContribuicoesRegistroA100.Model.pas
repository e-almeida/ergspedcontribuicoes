unit SPEDContribuicoesRegistroA100.Model;

interface

uses SPEDContribuicoesRegistroA100.Model.Interf,
  VWSPEDCONTRIBREGISTROA100.Entidade.Model,
  ormbr.container.dataset.interfaces,
  ormbr.factory.interfaces,
  Data.DB,
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option,
  FireDAC.Stan.Error,
  FireDAC.UI.Intf,
  FireDAC.Phys.Intf,
  FireDAC.Stan.Def,
  FireDAC.Stan.Pool,
  FireDAC.Stan.Async,
  FireDAC.Phys,
  FireDAC.Phys.FB,
  FireDAC.Phys.FBDef,
  FireDAC.VCLUI.Wait,
  FireDAC.Comp.Client,
  FireDAC.Phys.IBBase,
  FireDAC.Comp.UI,
  FireDAC.Stan.Param,
  FireDAC.DatS,
  FireDAC.DApt.Intf,
  FireDAC.Comp.dataset;

type
  TSPEDContribuicoesRegistroA100Model = class(TInterfacedObject,
    ISPEDContribuicoesRegistroA100Model)
  private
    FConexao: IDBConnection;
    FDAO: IContainerDataSet<TVWSPEDCONTRIBREGISTROA100>;
    FQuery: TFDMemTable;
  public
    constructor create;
    destructor Destroy; override;

    class function New: ISPEDContribuicoesRegistroA100Model;

    function Execute(AValue: string): ISPEDContribuicoesRegistroA100Model;
    function Lista: TFDMemTable;
    function DAO: IContainerDataSet<TVWSPEDCONTRIBREGISTROA100>;
  end;

implementation

{ TSPEDContribuicoesRegistroA100Model }

uses ERGSped.Model,
  ormbr.container.fdmemtable,
  ormbr.dml.generator.firebird,
  ormbr.types.database;

constructor TSPEDContribuicoesRegistroA100Model.create;
begin
  FConexao := TERGSPEDModel.New.Conexao.FireDAC.firebird.Conexao;
  FQuery := TFDMemTable.create(nil);
  FDAO := TContainerFDMemTable<TVWSPEDCONTRIBREGISTROA100>.create
    (FConexao, FQuery);
end;

function TSPEDContribuicoesRegistroA100Model.DAO
  : IContainerDataSet<TVWSPEDCONTRIBREGISTROA100>;
begin
  Result := FDAO;
end;

destructor TSPEDContribuicoesRegistroA100Model.Destroy;
begin

  inherited;
end;

function TSPEDContribuicoesRegistroA100Model.Execute(AValue: string)
  : ISPEDContribuicoesRegistroA100Model;
begin
  Result := Self;
  FConexao.ExecuteDirect(AValue);
end;

function TSPEDContribuicoesRegistroA100Model.Lista: TFDMemTable;
begin
  Result := FQuery;
end;

class function TSPEDContribuicoesRegistroA100Model.New
  : ISPEDContribuicoesRegistroA100Model;
begin
  Result := Self.create
end;

end.
