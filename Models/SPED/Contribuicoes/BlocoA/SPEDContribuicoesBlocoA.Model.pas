unit SPEDContribuicoesBlocoA.Model;

interface

uses SPEDContribuicoesBlocoA.Model.Interf, ACBrEPCBloco_A_Class,
  SPEDContribuicoesRegistroA100.Model.Interf,  ACBrEPCBlocos;

type
  TSPEDContribuicoesBlocoAModel = class(TInterfacedObject,
    ISPEDContribuicoesBlocoAModel)
  private
    FEmpresa: Integer;
    FDataInicial: TDate;
    FDataFinal: TDate;
    FBlocoA: TBloco_A;
    FRegistroA100: ISPEDContribuicoesRegistroA100Model;
  public
    constructor create;
    destructor Destroy; override;

    class function New: ISPEDContribuicoesBlocoAModel;

    function Empresa(AValue: Integer): ISPEDContribuicoesBlocoAModel;
    function DataInicial(AValue: TDate): ISPEDContribuicoesBlocoAModel;
    function DataFinal(AValue: TDate): ISPEDContribuicoesBlocoAModel;
    Function RegistroA001: ISPEDContribuicoesBlocoAModel;
    Function RegistroA010: ISPEDContribuicoesBlocoAModel;
    Function RegistroA100: ISPEDContribuicoesBlocoAModel;
    Function RegistroA170: ISPEDContribuicoesBlocoAModel;
    function &End: TBloco_A;
  end;

implementation

{ TSPEDContribuicoesBlocoAModel }

uses SPEDContribuicoesRegistroA100.Model, System.SysUtils;

function TSPEDContribuicoesBlocoAModel.&End: TBloco_A;
begin
  with FBlocoA.RegistroA001New do
  begin
    if Not(FRegistroA100.Lista.RecordCount<=0) then begin
     IND_MOV := imComDados;
    end else begin
     IND_MOV := imSemDados;
    end;

    with FBlocoA.RegistroA010New do
     begin
      CNPJ := '';

        FRegistroA100.Lista.First;
        while Not(FRegistroA100.Lista.Eof) do
        begin
          with FBlocoA.RegistroA100New do
           begin
             IND_OPER      := itoContratado;
             IND_EMIT      := iedfProprio;
             COD_PART      := FRegistroA100.Lista.FieldByName('COD_PART').AsString;
             COD_SIT       := TACBrCodSit(FRegistroA100.Lista.FieldByName('COD_SIT').AsInteger);
             SER           := '';
             SUB           := '';
             NUM_DOC       := FRegistroA100.Lista.FieldByName('NUM_DOC').AsString;
             CHV_NFSE      := '';
             DT_DOC        := FRegistroA100.Lista.FieldByName('DT_DOC').AsDateTime;
             DT_EXE_SERV   := FRegistroA100.Lista.FieldByName('DT_EXE_SERV').AsDateTime;
             VL_DOC        := FRegistroA100.Lista.FieldByName('VL_DOC').AsCurrency;
             IND_PGTO      := TACBrIndPgto(FRegistroA100.Lista.FieldByName('COD_SIT').AsInteger);
             VL_DESC       := FRegistroA100.Lista.FieldByName('VL_DESC').AsCurrency;
             VL_BC_PIS     := FRegistroA100.Lista.FieldByName('VL_BC_PIS').AsCurrency;
             VL_PIS        := FRegistroA100.Lista.FieldByName('VL_PIS').AsCurrency;
             VL_BC_COFINS  := FRegistroA100.Lista.FieldByName('VL_BC_COFINS').AsCurrency;
             VL_COFINS     := FRegistroA100.Lista.FieldByName('VL_COFINS').AsCurrency;
             VL_PIS_RET    := FRegistroA100.Lista.FieldByName('VL_PIS_RET').AsCurrency;
             VL_COFINS_RET := FRegistroA100.Lista.FieldByName('VL_COFINS_RET').AsCurrency;
             VL_ISS        := FRegistroA100.Lista.FieldByName('VL_ISS').AsCurrency;
           end;
           FRegistroA100.Lista.Next;
        end;
     end;
  end;

  Result := FBlocoA;
end;

constructor TSPEDContribuicoesBlocoAModel.create;
begin
  FBlocoA := TBloco_A.Create;
  FBlocoA.Delimitador := '|';
end;

function TSPEDContribuicoesBlocoAModel.DataFinal(
  AValue: TDate): ISPEDContribuicoesBlocoAModel;
begin
  Result := Self;
  FDataFinal := AValue;
end;

function TSPEDContribuicoesBlocoAModel.DataInicial(
  AValue: TDate): ISPEDContribuicoesBlocoAModel;
begin
  Result := Self;
  FDataInicial := AValue;
end;

destructor TSPEDContribuicoesBlocoAModel.Destroy;
begin

  inherited;
end;

function TSPEDContribuicoesBlocoAModel.Empresa(
  AValue: Integer): ISPEDContribuicoesBlocoAModel;
begin
   Result   := Self;
   FEmpresa := AValue;
end;

class function TSPEDContribuicoesBlocoAModel.New: ISPEDContribuicoesBlocoAModel;
begin
  Result := Self.create
end;

function TSPEDContribuicoesBlocoAModel.RegistroA001: ISPEDContribuicoesBlocoAModel;
begin
  Result := Self;
end;

function TSPEDContribuicoesBlocoAModel.RegistroA010: ISPEDContribuicoesBlocoAModel;
begin
  Result := Self;
end;

function TSPEDContribuicoesBlocoAModel.RegistroA100: ISPEDContribuicoesBlocoAModel;
begin
   Result        := Self;
   FRegistroA100 := TSPEDContribuicoesRegistroA100Model.New;

  FRegistroA100.Execute('Delete from TSPEDCONTRIBREGISTROA100');
  FRegistroA100.Execute('execute procedure ajustaspedcontribregistroA100('
                       +IntToStr(FEmpresa)      +',  '''
                       +FormatDateTime('dd.mm.yyyy', FDataInicial) +''', '''
                       +FormatDateTime('dd.mm.yyyy', FDataFinal)+''')');

   FRegistroA100.DAO.Open;
end;

function TSPEDContribuicoesBlocoAModel.RegistroA170: ISPEDContribuicoesBlocoAModel;
begin
  Result := Self;

end;

end.
