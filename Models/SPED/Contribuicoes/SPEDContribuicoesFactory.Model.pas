unit SPEDContribuicoesFactory.Model;

interface

uses
  SPEDContribuicoesFactory.Model.Interf,
  SPEDContribuicoesBloco0.Model.Interf, SPEDContribuicoesBlocoA.Model.Interf;

type
  TSPEDContribuicoesFactoryModel = class(TInterfacedObject,
    ISPEDContribuicoesFactoryModel)
  private

  public
    constructor create;
    destructor Destroy; override;

    class function New: ISPEDContribuicoesFactoryModel;

    function Bloco0: ISPEDContribuicoesBloco0Model;
    function BlocoA: ISPEDContribuicoesBlocoAModel;
  end;

implementation

{ TSPEDContribuicoesFactoryModel }

uses SPEDContribuicoesBloco0.Model, SPEDContribuicoesBlocoA.Model;

function TSPEDContribuicoesFactoryModel.Bloco0: ISPEDContribuicoesBloco0Model;
begin
  Result := TSPEDContribuicoesBloco0Model.New;
end;

function TSPEDContribuicoesFactoryModel.BlocoA: ISPEDContribuicoesBlocoAModel;
begin
  Result := TSPEDContribuicoesBlocoAModel.New;
end;

constructor TSPEDContribuicoesFactoryModel.create;
begin

end;

destructor TSPEDContribuicoesFactoryModel.Destroy;
begin

  inherited;
end;

class function TSPEDContribuicoesFactoryModel.New
  : ISPEDContribuicoesFactoryModel;
begin
  Result := Self.create
end;

end.
