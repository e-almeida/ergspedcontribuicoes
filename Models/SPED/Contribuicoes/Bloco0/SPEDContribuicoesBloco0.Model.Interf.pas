unit SPEDContribuicoesBloco0.Model.Interf;

interface

uses
  ACBrEPCBloco_0_Class;

type
 ISPEDContribuicoesBloco0Model = interface
   ['{C99171E1-D2ED-4FBD-A4AB-0E352B0BA09A}']
   function Empresa(AValue: Integer): ISPEDContribuicoesBloco0Model;
   function DataInicial(AValue: TDate): ISPEDContribuicoesBloco0Model;
   function DataFinal(AValue: TDate): ISPEDContribuicoesBloco0Model;
   Function Registro0000: ISPEDContribuicoesBloco0Model;
   Function Registro0001: ISPEDContribuicoesBloco0Model;
   Function Registro0100: ISPEDContribuicoesBloco0Model;
   Function Registro0110: ISPEDContribuicoesBloco0Model;
   Function Registro0140: ISPEDContribuicoesBloco0Model;
   Function Registro0145: ISPEDContribuicoesBloco0Model;
   Function Registro0150: ISPEDContribuicoesBloco0Model;
   Function Registro0190: ISPEDContribuicoesBloco0Model;
   Function Registro0200: ISPEDContribuicoesBloco0Model;
   Function Registro0205: ISPEDContribuicoesBloco0Model;
   Function Registro0500: ISPEDContribuicoesBloco0Model;
   function &End: TBloco_0;
 end;

implementation

end.
