unit SPEDContribuicoesRegistro0200.Model;

interface

uses
  SPEDContribuicoesRegistro0200.Model.Interf,
  VWSPEDCONTRIBREGISTRO0200.Entidade.Model,

  System.Generics.Collections,

  ormbr.factory.interfaces,
  ormbr.container.dataset.interfaces,

  Data.DB,
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option,
  FireDAC.Stan.Error,
  FireDAC.UI.Intf,
  FireDAC.Phys.Intf,
  FireDAC.Stan.Def,
  FireDAC.Stan.Pool,
  FireDAC.Stan.Async,
  FireDAC.Phys,
  FireDAC.Phys.FB,
  FireDAC.Phys.FBDef,
  FireDAC.VCLUI.Wait,
  FireDAC.Comp.Client,
  FireDAC.Phys.IBBase,
  FireDAC.Comp.UI,
  FireDAC.Stan.Param,
  FireDAC.DatS,
  FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet;

type
  TSPEDContribuicoesRegistro0200Model = class(TInterfacedObject,
    ISPEDContribuicoesRegistro0200Model)
  private
    FConexao : IDBConnection;
    FDAO     : IContainerDataSet<TVWSPEDCONTRIBREGISTRO0200>;
    FQuery   : TFDMemTable;
  public
    constructor create;
    destructor Destroy; override;

    class function New: ISPEDContribuicoesRegistro0200Model;

    function Execute(AValue: string): ISPEDContribuicoesRegistro0200Model;
    function Lista: TFDMemTable;
    function DAO: IContainerDataSet<TVWSPEDCONTRIBREGISTRO0200>;

  end;

implementation

{ TSPEDContribuicoesRegistro0200Model }

uses
  ERGSped.Model,
  ormbr.container.fdmemtable,
  ormbr.dml.generator.firebird,
  ormbr.types.database;

constructor TSPEDContribuicoesRegistro0200Model.create;
begin
  FConexao := TERGSPEDModel.New.Conexao.FireDAC.Firebird.Conexao;
  FQuery   := TFDMemTable.Create(nil);
  FDAO     := TContainerFDMemTable<TVWSPEDCONTRIBREGISTRO0200>.Create(FConexao, FQuery);
end;

function TSPEDContribuicoesRegistro0200Model.DAO: IContainerDataSet<TVWSPEDCONTRIBREGISTRO0200>;
begin
  Result := FDAO;
end;

destructor TSPEDContribuicoesRegistro0200Model.Destroy;
begin

  inherited;
end;

function TSPEDContribuicoesRegistro0200Model.Execute(
  AValue: string): ISPEDContribuicoesRegistro0200Model;
begin
  Result := Self;
  FConexao.ExecuteDirect(AValue);
end;

function TSPEDContribuicoesRegistro0200Model.Lista: TFDMemTable;
begin
  Result := FQuery;
end;

class function TSPEDContribuicoesRegistro0200Model.New
  : ISPEDContribuicoesRegistro0200Model;
begin
  Result := Self.create
end;

end.
