unit SPEDContribuicoesRegistro0110.Model;

interface

uses
  SPEDContribuicoesRegistro0110.Model.Interf,
  System.Generics.Collections,
  VWSPEDCONTRIBREGISTRO0110.Entidade.Model,
  ormbr.factory.interfaces,
  ormbr.container.objectset.interfaces;

type
  TSPEDContribuicoesRegistro0110Model = class(TInterfacedObject,
    ISPEDContribuicoesRegistro0110Model)
  private
    FConexao: IDBConnection;
    FDAO: IContainerObjectSet<TVWSPEDCONTRIBREGISTRO0110>;
  public
    constructor create;
    destructor Destroy; override;

    class function New: ISPEDContribuicoesRegistro0110Model;

    function Lista: TObjectList<TVWSPEDCONTRIBREGISTRO0110>;
  end;

implementation

{ TSPEDContribuicoesRegistro0110Model }

uses ERGSped.Model, ormbr.container.objectset;

constructor TSPEDContribuicoesRegistro0110Model.create;
begin
  FConexao := TERGSPEDModel.New.Conexao.FireDAC.Firebird.Conexao;
  FDAO := TContainerObjectSet<TVWSPEDCONTRIBREGISTRO0110>.create(FConexao, 10);
end;

destructor TSPEDContribuicoesRegistro0110Model.Destroy;
begin

  inherited;
end;

function TSPEDContribuicoesRegistro0110Model.Lista
  : TObjectList<TVWSPEDCONTRIBREGISTRO0110>;
begin
  Result := FDAO.FindWhere('CODIGO = 1', 'NOME');
end;

class function TSPEDContribuicoesRegistro0110Model.New
  : ISPEDContribuicoesRegistro0110Model;
begin
  Result := Self.create
end;

end.
