unit SPEDContribuicoesRegistro0110.Model.Interf;

interface

uses VWSPEDCONTRIBREGISTRO0110.Entidade.Model, System.Generics.Collections;

type
  ISPEDContribuicoesRegistro0110Model = interface
    ['{635579B0-3B39-4FD4-BECB-E746CB3FDC17}']
    function Lista: TObjectList<TVWSPEDCONTRIBREGISTRO0110>;
  end;

implementation

end.
