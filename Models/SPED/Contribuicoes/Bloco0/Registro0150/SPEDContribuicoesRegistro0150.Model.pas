unit SPEDContribuicoesRegistro0150.Model;

interface

uses
  SPEDContribuicoesRegistro0150.Model.Interf,
  VWSPEDCONTRIBREGISTRO0150.Entidade.Model,
  Data.DB,
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option,
  FireDAC.Stan.Error,
  FireDAC.UI.Intf,
  FireDAC.Phys.Intf,
  FireDAC.Stan.Def,
  FireDAC.Stan.Pool,
  FireDAC.Stan.Async,
  FireDAC.Phys,
  FireDAC.Phys.FB,
  FireDAC.Phys.FBDef,
  FireDAC.VCLUI.Wait,
  FireDAC.Comp.Client,
  FireDAC.Phys.IBBase,
  FireDAC.Comp.UI,
  FireDAC.Stan.Param,
  FireDAC.DatS,
  FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet,
  ormbr.factory.interfaces,
  ormbr.container.dataset.interfaces;

type
  TSPEDContribuicoesRegistro0150Model = class(TInterfacedObject,
    ISPEDContribuicoesRegistro0150Model)
  private
    FConexao: IDBConnection;
    FDAO: IContainerDataSet<TVWSPEDCONTRIBREGISTRO0150>;
    FQuery: TFDMemTable;
  public
    constructor create;
    destructor Destroy; override;

    class function New: ISPEDContribuicoesRegistro0150Model;

    function Execute(AValue: string): ISPEDContribuicoesRegistro0150Model;
    function Lista: TFDMemTable;
    function DAO: IContainerDataSet<TVWSPEDCONTRIBREGISTRO0150>;

  end;

implementation

{ TSPEDContribuicoesRegistro0150Model }

uses
  ERGSped.Model,
  ormbr.container.fdmemtable,
  ormbr.dml.generator.firebird,
  ormbr.types.database;

constructor TSPEDContribuicoesRegistro0150Model.create;
begin
  FConexao := TERGSPEDModel.New.Conexao.FireDAC.Firebird.Conexao;
  FQuery   := TFDMemtable.Create(nil);
  FDAO     := TContainerFDMemTable<TVWSPEDCONTRIBREGISTRO0150>.create(FConexao, FQuery);
end;

function TSPEDContribuicoesRegistro0150Model.DAO: IContainerDataSet<TVWSPEDCONTRIBREGISTRO0150>;
begin
   Result := FDAO;
end;

destructor TSPEDContribuicoesRegistro0150Model.Destroy;
begin

  inherited;
end;

function TSPEDContribuicoesRegistro0150Model.Execute(
  AValue: string): ISPEDContribuicoesRegistro0150Model;
begin
  Result := Self;
  FConexao.ExecuteDirect(AValue);
end;

function TSPEDContribuicoesRegistro0150Model.Lista
  : TFDMemTable;
begin
  Result := FQuery;
end;

class function TSPEDContribuicoesRegistro0150Model.New
  : ISPEDContribuicoesRegistro0150Model;
begin
  Result := Self.create
end;

end.
