unit SPEDContribuicoesRegistro0150.Model.Interf;

interface

uses
  System.Generics.Collections,
  VWSPEDCONTRIBREGISTRO0150.Entidade.Model,
  ormbr.container.dataset.interfaces,
  Data.DB,
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option,
  FireDAC.Stan.Error,
  FireDAC.UI.Intf,
  FireDAC.Phys.Intf,
  FireDAC.Stan.Def,
  FireDAC.Stan.Pool,
  FireDAC.Stan.Async,
  FireDAC.Phys,
  FireDAC.Phys.FB,
  FireDAC.Phys.FBDef,
  FireDAC.VCLUI.Wait,
  FireDAC.Comp.Client,
  FireDAC.Phys.IBBase,
  FireDAC.Comp.UI,
  FireDAC.Stan.Param,
  FireDAC.DatS,
  FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet;

Type
  ISPEDContribuicoesRegistro0150Model = interface
    ['{D37ACF36-437A-4634-A80E-55B66C377218}']
    function Execute(AValue: string): ISPEDContribuicoesRegistro0150Model;
    function Lista: TFDMemTable;
    function DAO: IContainerDataSet<TVWSPEDCONTRIBREGISTRO0150>;
  end;

implementation

end.
