unit SPEDContribuicoesRegistro0140.Model.Interf;

interface

uses VWSPEDCONTRIBREGISTRO0140.Entidade.Model, System.Generics.Collections;

type
  ISPEDContribuicoesRegistro0140Model = interface
    ['{635579B0-3B39-4FD4-BECB-E746CB3FDC17}']
    function Lista: TObjectList<TVWSPEDCONTRIBREGISTRO0140>;
  end;

implementation

end.
