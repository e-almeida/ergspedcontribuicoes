unit SPEDContribuicoesRegistro0140.Model;

interface

uses
  SPEDContribuicoesRegistro0140.Model.Interf,
  System.Generics.Collections,
  VWSPEDCONTRIBREGISTRO0140.Entidade.Model,
  ormbr.factory.interfaces,
  ormbr.container.objectset.interfaces;

type
  TSPEDContribuicoesRegistro0140Model = class(TInterfacedObject,
    ISPEDContribuicoesRegistro0140Model)
  private
    FConexao: IDBConnection;
    FDAO: IContainerObjectSet<TVWSPEDCONTRIBREGISTRO0140>;
  public
    constructor create;
    destructor Destroy; override;

    class function New: ISPEDContribuicoesRegistro0140Model;

    function Lista: TObjectList<TVWSPEDCONTRIBREGISTRO0140>;
  end;

implementation

{ TSPEDContribuicoesRegistro0140Model }

uses ERGSped.Model, ormbr.container.objectset;

constructor TSPEDContribuicoesRegistro0140Model.create;
begin
  FConexao := TERGSPEDModel.New.Conexao.FireDAC.Firebird.Conexao;
  FDAO := TContainerObjectSet<TVWSPEDCONTRIBREGISTRO0140>.create(FConexao, 10);
end;

destructor TSPEDContribuicoesRegistro0140Model.Destroy;
begin

  inherited;
end;

function TSPEDContribuicoesRegistro0140Model.Lista
  : TObjectList<TVWSPEDCONTRIBREGISTRO0140>;
begin
  Result := FDAO.FindWhere('CODIGO = 1', 'NOME');
end;

class function TSPEDContribuicoesRegistro0140Model.New
  : ISPEDContribuicoesRegistro0140Model;
begin
  Result := Self.create
end;

end.
