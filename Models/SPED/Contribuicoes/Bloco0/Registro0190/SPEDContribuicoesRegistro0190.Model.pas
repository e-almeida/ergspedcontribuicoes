unit SPEDContribuicoesRegistro0190.Model;

interface

uses
  SPEDContribuicoesRegistro0190.Model.Interf,
  System.generics.Collections,
  VWSPEDCONTRIBREGISTRO0190.Entidade.Model,
  ormbr.factory.interfaces,
  ormbr.container.dataset.interfaces,
  Data.DB,
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option,
  FireDAC.Stan.Error,
  FireDAC.UI.Intf,
  FireDAC.Phys.Intf,
  FireDAC.Stan.Def,
  FireDAC.Stan.Pool,
  FireDAC.Stan.Async,
  FireDAC.Phys,
  FireDAC.Phys.FB,
  FireDAC.Phys.FBDef,
  FireDAC.VCLUI.Wait,
  FireDAC.Comp.Client,
  FireDAC.Phys.IBBase,
  FireDAC.Comp.UI,
  FireDAC.Stan.Param,
  FireDAC.DatS,
  FireDAC.DApt.Intf,
  FireDAC.Comp.dataset;

type
  TSPEDContribuicoesRegistro0190Model = class(TInterfacedObject,
    ISPEDContribuicoesRegistro0190Model)
  private
    FConexao: IDBConnection;
    FDAO: IContainerDataset<TVWSPEDCONTRIBREGISTRO0190>;
    FQuery: TFDMemTable;
  public
    constructor create;
    destructor Destroy; override;

    class function New: ISPEDContribuicoesRegistro0190Model;

    function Execute(AValue: string): ISPEDContribuicoesRegistro0190Model;
    function Lista: TFDMemTable;
    function DAO: IContainerDataset<TVWSPEDCONTRIBREGISTRO0190>;

  end;

implementation

{ TSPEDContribuicoesRegistro0190Model }

uses
  ERGSped.Model,
  ormbr.container.fdmemtable,
  ormbr.dml.generator.firebird,
  ormbr.types.database;

constructor TSPEDContribuicoesRegistro0190Model.create;
begin
  FConexao := TERGSPEDModel.New.Conexao.FireDAC.firebird.Conexao;
  FQuery := TFDMemTable.create(nil);
  FDAO := TContainerFDMemTable<TVWSPEDCONTRIBREGISTRO0190>.create
    (FConexao, FQuery);
end;

function TSPEDContribuicoesRegistro0190Model.DAO
  : IContainerDataset<TVWSPEDCONTRIBREGISTRO0190>;
begin
  Result := FDAO;
end;

destructor TSPEDContribuicoesRegistro0190Model.Destroy;
begin

  inherited;
end;

function TSPEDContribuicoesRegistro0190Model.Execute(AValue: string)
  : ISPEDContribuicoesRegistro0190Model;
begin
  Result := Self;
  FConexao.ExecuteDirect(AValue);
end;

function TSPEDContribuicoesRegistro0190Model.Lista: TFDMemTable;
begin
  Result := FQuery;
end;

class function TSPEDContribuicoesRegistro0190Model.New
  : ISPEDContribuicoesRegistro0190Model;
begin
  Result := Self.create
end;

end.
