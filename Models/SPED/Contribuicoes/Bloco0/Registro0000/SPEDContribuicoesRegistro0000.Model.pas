unit SPEDContribuicoesRegistro0000.Model;

interface

uses
  SPEDContribuicoesRegistro0000.Model.Interf,
  VWSPEDCONTRIBREGISTRO0000.Entidade.Model,

  //ORMBr
  ormbr.container.dataset.interfaces,
  ormbr.container.objectset.interfaces,
  ormbr.factory.interfaces,

  System.Generics.Collections;

type
  TSPEDContribuicoesRegistro0000Model = class(TInterfacedObject,
    ISPEDContribuicoesRegistro0000Model)
  private
    FConexao: IDBConnection;
    FDAO: IContainerObjectSet<TVWSPEDCONTRIBREGISTRO0000>;
  public
    constructor create;
    destructor Destroy; override;

    class function New: ISPEDContribuicoesRegistro0000Model;

    function Lista: TObjectList<TVWSPEDCONTRIBREGISTRO0000>;

  end;

implementation

{ TSPEDContribuicoesRegistro0000Model }

uses ERGSped.Model, ormbr.container.objectset;

constructor TSPEDContribuicoesRegistro0000Model.create;
begin
  FConexao := TERGSPEDModel.New.Conexao.FireDAC.Firebird.Conexao;
  FDAO     := TContainerObjectSet<TVWSPEDCONTRIBREGISTRO0000>.create(FConexao, 10);
end;

destructor TSPEDContribuicoesRegistro0000Model.Destroy;
begin

  inherited;
end;

function TSPEDContribuicoesRegistro0000Model.Lista: TObjectList<TVWSPEDCONTRIBREGISTRO0000>;
begin
   Result := FDAO.FindWhere('CODIGO = 1', 'NOME');
end;

class function TSPEDContribuicoesRegistro0000Model.New
  : ISPEDContribuicoesRegistro0000Model;
begin
  Result := Self.create
end;

end.
