unit SPEDContribuicoesRegistro0000.Model.Interf;

interface

uses
  VWSPEDCONTRIBREGISTRO0000.Entidade.Model,
  System.Generics.Collections;

type
  ISPEDContribuicoesRegistro0000Model = interface
    ['{86E02FCC-F902-4DF4-86F0-946D204DEDD7}']
    function Lista: TObjectList<TVWSPEDCONTRIBREGISTRO0000>;
  end;

implementation

end.
