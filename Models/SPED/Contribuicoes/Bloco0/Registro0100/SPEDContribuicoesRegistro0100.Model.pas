unit SPEDContribuicoesRegistro0100.Model;

interface

uses
  SPEDContribuicoesRegistro0100.Model.Interf,
  System.Generics.Collections,
  VWSPEDCONTRIBREGISTRO0100.Entidade.Model,

  ormbr.Factory.interfaces,
  ormbr.container.objectset.interfaces;

type
  TSPEDContribuicoesRegistro0100Model = class(TInterfacedObject,
    ISPEDContribuicoesRegistro0100Model)
  private
    FConexao: IDBConnection;
    FDAO: IContainerObjectSet<TVWSPEDCONTRIBREGISTRO0100>;
  public
    constructor create;
    destructor Destroy; override;

    class function New: ISPEDContribuicoesRegistro0100Model;

    function Lista: TObjectList<TVWSPEDCONTRIBREGISTRO0100>;

  end;

implementation

{ TSPEDContribuicoesRegistro0100Model }

uses ERGSped.Model, ormbr.container.objectset;

constructor TSPEDContribuicoesRegistro0100Model.create;
begin
  FConexao := TERGSPEDModel.New.Conexao.FireDAC.Firebird.Conexao;
  FDAO := TContainerObjectSet<TVWSPEDCONTRIBREGISTRO0100>.create(FConexao, 10);
end;

destructor TSPEDContribuicoesRegistro0100Model.Destroy;
begin

  inherited;
end;

function TSPEDContribuicoesRegistro0100Model.Lista: TObjectList<TVWSPEDCONTRIBREGISTRO0100>;
begin
  Result := FDAO.FindWhere('CODIGO = 1', 'NOME');
end;

class function TSPEDContribuicoesRegistro0100Model.New
  : ISPEDContribuicoesRegistro0100Model;
begin
  Result := Self.create
end;

end.
