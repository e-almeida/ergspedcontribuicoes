unit SPEDContribuicoesRegistro0100.Model.Interf;

interface

uses
  VWSPEDCONTRIBREGISTRO0100.Entidade.Model,
  System.Generics.Collections;

type
  ISPEDContribuicoesRegistro0100Model = interface
    ['{512F075D-06B1-4F4A-9B17-F4FB320D5EBF}']
    function Lista: TObjectList<TVWSPEDCONTRIBREGISTRO0100>;
  end;

implementation

end.
