unit SPEDContribuicoesBloco0.Model;

interface

uses
  SPEDContribuicoesBloco0.Model.Interf,
  ACBrEPCBloco_0_Class,
  ACBrEPCBlocos,
  System.SysUtils,
  ContribConfiguracoesBloco0Registro0000.Model.Interf,
  SPEDContribuicoesRegistro0000.Model.Interf,
  ContribConfiguracoesBloco0Registro0001.Model.Interf,
  SPEDContribuicoesRegistro0100.Model.Interf,
  ContribConfiguracoesBloco0Registro0110.Model.Interf,
  SPEDContribuicoesRegistro0140.Model.Interf,
  SPEDContribuicoesRegistro0150.Model.Interf,
  SPEDContribuicoesRegistro0190.Model.Interf,
  SPEDContribuicoesRegistro0200.Model.Interf,
  ContribConfiguracoesBloco0Registro0500.Model.Interf;

type
  TSPEDContribuicoesBloco0Model = class(TInterfacedObject,
    ISPEDContribuicoesBloco0Model)
  private
    FBloco0: TBloco_0;
    FEmpresa            : Integer;
    FDataInicial        : TDate;
    FDataFinal          : TDate;
    FConfigRegistro0000 : IContribConfiguracoesBloco0Registro0000Model;
    FConfigRegistro0001 : IContribConfiguracoesBloco0Registro0001Model;
    FConfigRegistro0110 : IContribConfiguracoesBloco0Registro0110Model;
    FConfigRegistro0500 : IContribConfiguracoesBloco0Registro0500Model;
    FRegistro0000       : ISPEDContribuicoesRegistro0000Model;
    FRegistro0100       : ISPEDContribuicoesRegistro0100Model;
    FRegistro0140       : ISPEDContribuicoesRegistro0140Model;
    FRegistro0150       : ISPEDContribuicoesRegistro0150Model;
    FRegistro0190       : ISPEDContribuicoesRegistro0190Model;
    FRegistro0200       : ISPEDContribuicoesRegistro0200Model;
  public
    constructor create;
    destructor Destroy; override;

    class function New: ISPEDContribuicoesBloco0Model;

    function Empresa(AValue: Integer)   : ISPEDContribuicoesBloco0Model;
    function DataInicial(AValue: TDate) : ISPEDContribuicoesBloco0Model;
    function DataFinal(AValue: TDate)   : ISPEDContribuicoesBloco0Model;
    Function Registro0000: ISPEDContribuicoesBloco0Model;
    Function Registro0001: ISPEDContribuicoesBloco0Model;
    Function Registro0100: ISPEDContribuicoesBloco0Model;
    Function Registro0110: ISPEDContribuicoesBloco0Model;
    Function Registro0140: ISPEDContribuicoesBloco0Model;
    Function Registro0145: ISPEDContribuicoesBloco0Model;
    Function Registro0150: ISPEDContribuicoesBloco0Model;
    Function Registro0190: ISPEDContribuicoesBloco0Model;
    Function Registro0200: ISPEDContribuicoesBloco0Model;
    Function Registro0205: ISPEDContribuicoesBloco0Model;
    Function Registro0500: ISPEDContribuicoesBloco0Model;
    function &End: TBloco_0;
  end;

implementation

{ TSPEDContribuicoesBloco0Model }

uses ERGSped.Model,
  SPEDContribuicoesRegistro0000.Model,
  SPEDContribuicoesRegistro0100.Model,
  SPEDContribuicoesRegistro0140.Model,
  SPEDContribuicoesRegistro0150.Model,
  SPEDContribuicoesRegistro0190.Model,
  SPEDContribuicoesRegistro0200.Model,

  ormbr.mapping.explorer,
  ormbr.objects.helper;

function TSPEDContribuicoesBloco0Model.Empresa(
  AValue: Integer): ISPEDContribuicoesBloco0Model;
begin
  Result   := Self;
  FEmpresa := AValue;
end;

function TSPEDContribuicoesBloco0Model.&End: TBloco_0;
var
  int0140: integer;
  int0150: integer;
  int0190: integer;
  int0200: integer;
begin
  with FBloco0.Registro0000New do
  begin
    COD_VER          := TACBrCodVer(FConfigRegistro0000.COD_VER);
    TIPO_ESCRIT      := TACBrTipoEscrit(FConfigRegistro0000.TIPO_ESCRIT);
    IND_SIT_ESP      := TACBrIndSitEsp(FConfigRegistro0000.IND_SIT_ESP);
    NUM_REC_ANTERIOR := '';
    DT_INI           := FDataInicial;
    DT_FIN           := FDataFinal;
    NOME             := FRegistro0000.Lista.Items[0].NOME;
    CNPJ             := FRegistro0000.Lista.Items[0].CNPJ;
    UF               := FRegistro0000.Lista.Items[0].UF;
    COD_MUN          := FRegistro0000.Lista.Items[0].COD_MUN;
    SUFRAMA          := FRegistro0000.Lista.Items[0].SUFRAMA;
    IND_NAT_PJ       := TACBrIndNatPJ(FConfigRegistro0000.IND_NAT_PJ);
    IND_ATIV         := TACBrIndAtiv(FConfigRegistro0000.IND_ATIV);

    with FBloco0.Registro0001New do
    begin
      IND_MOV := TACBrIndMov(FConfigRegistro0001.IND_MOV);

      { 0100 - Identificação do contabilista responsável }
      with FBloco0.Registro0100New do
      begin
        NOME     := FRegistro0100.Lista.Items[0].NOME;
        CPF      := FRegistro0100.Lista.Items[0].CPF;
        CRC      := FRegistro0100.Lista.Items[0].CRC;
        CNPJ     := FRegistro0100.Lista.Items[0].CNPJ;
        CEP      := FRegistro0100.Lista.Items[0].CEP;
        ENDERECO := FRegistro0100.Lista.Items[0].ENDERECO;
        NUM      := FRegistro0100.Lista.Items[0].NUM;
        COMPL    := FRegistro0100.Lista.Items[0].COMPL;
        BAIRRO   := FRegistro0100.Lista.Items[0].BAIRRO;
        FONE     := FRegistro0100.Lista.Items[0].FONE;
        FAX      := FRegistro0100.Lista.Items[0].FAX;
        EMAIL    := FRegistro0100.Lista.Items[0].EMAIL;
        COD_MUN  := FRegistro0100.Lista.Items[0].COD_MUN;
      end;

      { 0110 - Regime de tributação }
      with FBloco0.Registro0110New do
      begin
        COD_INC_TRIB  := TACBrCodIncTrib(FConfigRegistro0110.COD_INC_TRIB);
        IND_APRO_CRED := TACBrIndAproCred(FConfigRegistro0110.IND_APRO_CRED);
        COD_TIPO_CONT := TACBrCodTipoCont(FConfigRegistro0110.COD_TIPO_CONT);

        if COD_INC_TRIB in [codEscrOpIncCumulativo, codEscrOpIncAmbos] then
        begin
          IND_REG_CUM := TACBrIndRegCum(FConfigRegistro0110.Ind_Reg_Cum);
        end;
      end;

      { 0140 - Identificação do estabelecimento }
      for int0140 := 1 to 1 do
      begin
        with FBloco0.Registro0140New do
        begin
          COD_EST := IntToStr(FRegistro0140.Lista.Items[0].CODIGO);
          NOME    := FRegistro0140.Lista.Items[0].NOME;
          CNPJ    := FRegistro0140.Lista.Items[0].CNPJ;
          UF      := FRegistro0140.Lista.Items[0].UF;
          IE      := FRegistro0140.Lista.Items[0].IE;
          COD_MUN := FRegistro0140.Lista.Items[0].COD_MUN;
          IM      := FRegistro0140.Lista.Items[0].IM;
          SUFRAMA := FRegistro0140.Lista.Items[0].SUFRAMA;

          { 0150 - Identificação dos Participantes }
          FRegistro0150.Lista.First;
          while not(FRegistro0150.Lista.Eof) do begin
            with FBloco0.Registro0150New do
            begin
              COD_PART := FRegistro0150.Lista.FieldByName('COD_PART').AsString;
              NOME     := FRegistro0150.Lista.FieldByName('NOME').AsString;
              COD_PAIS := IntToStr(FRegistro0150.Lista.FieldByName('COD_PAIS').AsInteger);

               if FRegistro0150.Lista.FieldByName('TIPO').AsString = 'J' then begin
                CNPJ     := FRegistro0150.Lista.FieldByName('CPFCNPJ').AsString;
                IE       := FRegistro0150.Lista.FieldByName('RGIE').AsString;
               end else begin
                CPF      := FRegistro0150.Lista.FieldByName('CPFCNPJ').AsString;
               end;

              COD_MUN  := FRegistro0150.Lista.FieldByName('COD_MUN').AsInteger;
              SUFRAMA  := FRegistro0150.Lista.FieldByName('SUFRAMA').AsString;
              ENDERECO := FRegistro0150.Lista.FieldByName('ENDERECO').AsString;
              NUM      := FRegistro0150.Lista.FieldByName('NUM').AsString;
              COMPL    := FRegistro0150.Lista.FieldByName('COMPL').AsString;
              BAIRRO   := FRegistro0150.Lista.FieldByName('BAIRRO').AsString;
            end;
            FRegistro0150.Lista.Next;
          end;


          { 0190 - Identificação das Unidades de Medida }
          FRegistro0190.Lista.First;
          while not(FRegistro0190.Lista.Eof) do begin
            with FBloco0.Registro0190New do
            begin
              UNID  :=  FRegistro0190.Lista.FieldByName('UNM').AsString;
              DESCR := FRegistro0190.Lista.FieldByName('DESCRICAO').AsString;
            end;
            FRegistro0190.Lista.Next;
          end;


           // 0200 - Tabela de Identificação do Item (Produtos e Serviços)
          FRegistro0200.Lista.First;
          while not(FRegistro0200.Lista.Eof) do begin
              with FBloco0.Registro0200New do begin
                COD_ITEM     := FRegistro0200.Lista.FieldByName('COD_ITEM').AsString;
                DESCR_ITEM   := FRegistro0200.Lista.FieldByName('DESCRICAO').AsString;
                COD_BARRA    := FRegistro0200.Lista.FieldByName('COD_BARRA').AsString;
                COD_ANT_ITEM := '';
                UNID_INV     := FRegistro0200.Lista.FieldByName('UNM').AsString;
                TIPO_ITEM    := TACBrTipoItem(StrToInt(FRegistro0200.Lista.FieldByName('TIPO').AsString));
                COD_NCM      := FRegistro0200.Lista.FieldByName('COD_NCM').AsString;
                EX_IPI       := FRegistro0200.Lista.FieldByName('EX_IPI').AsString;
                COD_GEN      := FRegistro0200.Lista.FieldByName('COD_GEN').AsString;
                COD_LST      := FRegistro0200.Lista.FieldByName('COD_LST').AsString;
                ALIQ_ICMS    := FRegistro0200.Lista.FieldByName('ALIQ_ICMS').AsCurrency;
              end;
            FRegistro0200.Lista.Next;
          end;
        end;
      end;

      with FBloco0.Registro0500New do begin
       DT_ALT      := StrToDate(FConfigRegistro0500.Dt_Alt);
       COD_NAT_CC  := TACBrNaturezaConta(FConfigRegistro0500.Cod_Nat_Cc);
       IND_CTA     := TACBrIndCTA(FConfigRegistro0500.Ind_Cta);
       NIVEL       := FConfigRegistro0500.Nivel;
       COD_CTA     := FConfigRegistro0500.Cod_Cta;
       NOME_CTA    := FConfigRegistro0500.Nome_Cta;
       COD_CTA_REF := FConfigRegistro0500.Cod_Cta_Ref;
       CNPJ_EST    := FConfigRegistro0500.CNPJ_Est;
      end;
    end;
  end;

  Result := FBloco0;
end;

constructor TSPEDContribuicoesBloco0Model.create;
begin
  FBloco0 := TBloco_0.create;
  FBloco0.Delimitador := '|';
end;

function TSPEDContribuicoesBloco0Model.DataFinal(
  AValue: TDate): ISPEDContribuicoesBloco0Model;
begin
  Result     := Self;
  FDataFinal := AValue;
end;

function TSPEDContribuicoesBloco0Model.DataInicial(
  AValue: TDate): ISPEDContribuicoesBloco0Model;
begin
 Result       := Self;
 FDataInicial := AValue;
end;

destructor TSPEDContribuicoesBloco0Model.Destroy;
begin

  inherited;
end;

class function TSPEDContribuicoesBloco0Model.New: ISPEDContribuicoesBloco0Model;
begin
  Result := Self.create
end;

function TSPEDContribuicoesBloco0Model.Registro0000
  : ISPEDContribuicoesBloco0Model;
begin
  Result := Self;

  FConfigRegistro0000 := TERGSPEDModel.New.Configuracoes.SPEDContribuicoes.
    Bloco0.Registro0000.PrepararConfiguracoes;

  FRegistro0000 := TSPEDContribuicoesRegistro0000Model.New;
end;

function TSPEDContribuicoesBloco0Model.Registro0001
  : ISPEDContribuicoesBloco0Model;
begin
  Result := Self;

  FConfigRegistro0001 := TERGSPEDModel.New.Configuracoes.SPEDContribuicoes.
    Bloco0.Registro0001.PrepararConfiguracoes;
end;

function TSPEDContribuicoesBloco0Model.Registro0100
  : ISPEDContribuicoesBloco0Model;
begin
  Result := Self;
  FRegistro0100 := TSPEDContribuicoesRegistro0100Model.New;
end;

function TSPEDContribuicoesBloco0Model.Registro0110
  : ISPEDContribuicoesBloco0Model;
begin
  Result := Self;

  FConfigRegistro0110 := TERGSPEDModel.New.Configuracoes.SPEDContribuicoes.
    Bloco0.Registro0110.PrepararConfiguracoes;
end;

function TSPEDContribuicoesBloco0Model.Registro0140
  : ISPEDContribuicoesBloco0Model;
begin
  Result := Self;

  FRegistro0140 := TSPEDContribuicoesRegistro0140Model.New;
end;

function TSPEDContribuicoesBloco0Model.Registro0145
  : ISPEDContribuicoesBloco0Model;
begin
  Result := Self;
end;

function TSPEDContribuicoesBloco0Model.Registro0150
  : ISPEDContribuicoesBloco0Model;
begin
  Result        := Self;
  FRegistro0150 := TSPEDContribuicoesRegistro0150Model.New;

  FRegistro0150.Execute('Delete from TSPEDCONTRIBREGISTRO0150');
  FRegistro0150.Execute('execute procedure ajustaspedcontribregistro0150('
                       +IntToStr(FEmpresa)      +',  '''
                       +FormatDateTime('dd.mm.yyyy', FDataInicial) +''', '''
                       +FormatDateTime('dd.mm.yyyy', FDataFinal)+''')');

 FRegistro0150.DAO.Open;
end;

function TSPEDContribuicoesBloco0Model.Registro0190
  : ISPEDContribuicoesBloco0Model;
begin
  Result := Self;
  FRegistro0190 := TSPEDContribuicoesRegistro0190Model.New;

  FRegistro0190.Execute('Delete from TSPEDCONTRIBREGISTRO0190');
  FRegistro0190.Execute('execute procedure ajustaspedcontribregistro0190('
                       +IntToStr(FEmpresa)      +',  '''
                       +FormatDateTime('dd.mm.yyyy', FDataInicial) +''', '''
                       +FormatDateTime('dd.mm.yyyy', FDataFinal)+''')');

 FRegistro0190.DAO.Open;
end;

function TSPEDContribuicoesBloco0Model.Registro0200
  : ISPEDContribuicoesBloco0Model;
begin
  Result := Self;

  FRegistro0200 := TSPEDContribuicoesRegistro0200Model.New;

  FRegistro0200.Execute('Delete from TSPEDCONTRIBREGISTRO0200');
  FRegistro0200.Execute('execute procedure ajustaspedcontribregistro0200('
                       +IntToStr(FEmpresa)      +',  '''
                       +FormatDateTime('dd.mm.yyyy', FDataInicial) +''', '''
                       +FormatDateTime('dd.mm.yyyy', FDataFinal)+''')');

  FRegistro0200.DAO.Open;
end;

function TSPEDContribuicoesBloco0Model.Registro0205
  : ISPEDContribuicoesBloco0Model;
begin
  Result := Self;
end;

function TSPEDContribuicoesBloco0Model.Registro0500
  : ISPEDContribuicoesBloco0Model;
begin
  Result := Self;

  FConfigRegistro0500 := TERGSPEDModel.New
                          .Configuracoes
                          .SPEDContribuicoes
                          .Bloco0
                          .Registro0500
                          .PrepararConfiguracoes;
end;

end.
