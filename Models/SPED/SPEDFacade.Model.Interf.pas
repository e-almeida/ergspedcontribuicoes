unit SPEDFacade.Model.Interf;

interface

uses SPEDContribuicoesFactory.Model.Interf;

type
  ISPEDFacadeModel = interface
    ['{5B1415C0-DEDF-4FED-A13D-E35B8B11E127}']
    function Contribuicoes: ISPEDContribuicoesFactoryModel;
  end;

implementation

end.
