unit ConfiguracoesFactory.Model.Interf;

interface

uses ConfiguracoesConexao.Model.Interf, ContribConfiguracoesFacade.Model.Interf;

Type
  IConfiguracoesFactoryModel = interface
    ['{FFD6124C-EB85-432B-83E2-B6D6EDB17041}']
    function Conexao: IConfiguracoesConexaoModel;
    function SPEDContribuicoes: IContribConfiguracoesFacadeModel;
  end;

implementation

end.
