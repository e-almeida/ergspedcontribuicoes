unit ContribConfiguracoesBloco0Registro0001.Model;

interface

uses
  ContribConfiguracoesBloco0Registro0001.Model.Interf,
  IniFiles, System.SysUtils, Vcl.Forms;

type
  TContribConfiguracoesBloco0Registro0001Model = class(TInterfacedObject,
    IContribConfiguracoesBloco0Registro0001Model)
  private
    FArquivo: string;
    FIniArquivo: TIniFile;

    procedure PreparaArquivoDeConfiguracoes;
    procedure ConfiguracoesPadraoRegistro0001;
  public
    constructor create;
    destructor Destroy; override;

    class function New: IContribConfiguracoesBloco0Registro0001Model;

    function PrepararConfiguracoes
      : IContribConfiguracoesBloco0Registro0001Model;

    function Ind_Mov: Integer;
  end;

implementation

{ TContribConfiguracoesBloco0Registro0001Model }

procedure TContribConfiguracoesBloco0Registro0001Model.
  ConfiguracoesPadraoRegistro0001;
begin
  FIniArquivo.WriteInteger('Registro0001', 'Ind_Mov', 0);
end;

constructor TContribConfiguracoesBloco0Registro0001Model.create;
begin
  FArquivo := ExtractFileDir(Application.ExeName) + '\' +'ERGSpedContribRegistro0001.ini';
  FIniArquivo := TIniFile.create(FArquivo);
end;

destructor TContribConfiguracoesBloco0Registro0001Model.Destroy;
begin

  inherited;
end;

function TContribConfiguracoesBloco0Registro0001Model.Ind_Mov: Integer;
begin
  Result := FIniArquivo.ReadInteger('Registro0001', 'Ind_Mov', 0);
end;

class function TContribConfiguracoesBloco0Registro0001Model.New
  : IContribConfiguracoesBloco0Registro0001Model;
begin
  Result := Self.create
end;

procedure TContribConfiguracoesBloco0Registro0001Model.
  PreparaArquivoDeConfiguracoes;
begin
  if not(FileExists(FArquivo)) then
    ConfiguracoesPadraoRegistro0001;
end;

function TContribConfiguracoesBloco0Registro0001Model.PrepararConfiguracoes
  : IContribConfiguracoesBloco0Registro0001Model;
begin
  Result := Self;
  PreparaArquivoDeConfiguracoes;
end;

end.
