unit ContribConfiguracoesBloco0Registro0110.Model.Interf;

interface

type
  IContribConfiguracoesBloco0Registro0110Model = interface
    ['{05123689-87BC-4226-8F47-07CC50CE9364}']
    function PrepararConfiguracoes: IContribConfiguracoesBloco0Registro0110Model;

    function Cod_Inc_Trib: Integer;
    function Ind_Apro_Cred: Integer;
    function Cod_Tipo_Cont: Integer;
    function Ind_Reg_Cum: Integer;
  end;

implementation

end.
