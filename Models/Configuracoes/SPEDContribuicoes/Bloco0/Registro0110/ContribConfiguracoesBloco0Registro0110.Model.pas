unit ContribConfiguracoesBloco0Registro0110.Model;

interface

uses
  ContribConfiguracoesBloco0Registro0110.Model.Interf,
  IniFiles, Vcl.Forms, System.SysUtils;

type
  TContribConfiguracoesBloco0Registro0110Model = class(TInterfacedObject,
    IContribConfiguracoesBloco0Registro0110Model)
  private
    FArquivo: string;
    FIniArquivo: TIniFile;

    procedure PreparaArquivoDeConfiguracoes;
    procedure ConfiguracoesPadraoRegistro0110;
  public
    constructor create;
    destructor Destroy; override;

    class function New: IContribConfiguracoesBloco0Registro0110Model;

    function PrepararConfiguracoes: IContribConfiguracoesBloco0Registro0110Model;

    function Cod_Inc_Trib: Integer;
    function Ind_Apro_Cred: Integer;
    function Cod_Tipo_Cont: Integer;
    function Ind_Reg_Cum: Integer;
  end;

implementation

{ TContribConfiguracoesBloco0Registro0110Model }

function TContribConfiguracoesBloco0Registro0110Model.Cod_Inc_Trib: Integer;
begin
  Result :=   FIniArquivo.ReadInteger('Registro0110', 'Cod_Inc_Trib', 0);
end;

function TContribConfiguracoesBloco0Registro0110Model.Cod_Tipo_Cont: Integer;
begin
  Result :=  FIniArquivo.ReadInteger('Registro0110', 'Cod_Tipo_Cont', 0);
end;

procedure TContribConfiguracoesBloco0Registro0110Model.ConfiguracoesPadraoRegistro0110;
begin
  FIniArquivo.WriteInteger('Registro0110', 'Cod_Inc_Trib', 0);
  FIniArquivo.WriteInteger('Registro0110', 'Ind_Apro_Cred', 0);
  FIniArquivo.WriteInteger('Registro0110', 'Cod_Tipo_Cont', 0);
  FIniArquivo.WriteInteger('Registro0110', 'Ind_Reg_Cum', 1);
end;

constructor TContribConfiguracoesBloco0Registro0110Model.create;
begin
  FArquivo := ExtractFileDir(Application.ExeName) + '\' +'ERGSpedContribRegistro0110.ini';
  FIniArquivo := TIniFile.create(FArquivo);
end;

destructor TContribConfiguracoesBloco0Registro0110Model.Destroy;
begin

  inherited;
end;

function TContribConfiguracoesBloco0Registro0110Model.Ind_Apro_Cred: Integer;
begin
 Result :=   FIniArquivo.ReadInteger('Registro0110', 'Ind_Apro_Cred', 0);
end;

function TContribConfiguracoesBloco0Registro0110Model.Ind_Reg_Cum: Integer;
begin
 Result :=   FIniArquivo.ReadInteger('Registro0110', 'Ind_Reg_Cum', 1);
end;

class function TContribConfiguracoesBloco0Registro0110Model.New
  : IContribConfiguracoesBloco0Registro0110Model;
begin
  Result := Self.create
end;

procedure TContribConfiguracoesBloco0Registro0110Model.PreparaArquivoDeConfiguracoes;
begin
  if Not(FileExists(FArquivo)) then
  ConfiguracoesPadraoRegistro0110;
end;

function TContribConfiguracoesBloco0Registro0110Model.PrepararConfiguracoes: IContribConfiguracoesBloco0Registro0110Model;
begin
  Result := Self;
  PreparaArquivoDeConfiguracoes;
end;

end.
