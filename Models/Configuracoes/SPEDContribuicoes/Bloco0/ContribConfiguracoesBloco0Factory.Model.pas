unit ContribConfiguracoesBloco0Factory.Model;

interface

uses
  ContribConfiguracoesBloco0Factory.Model.Interf,
  ContribConfiguracoesBloco0Registro0000.Model.Interf,
  ContribConfiguracoesBloco0Registro0001.Model.Interf,
  ContribConfiguracoesBloco0Registro0110.Model.Interf,
  ContribConfiguracoesBloco0Registro0500.Model.Interf;

type
  TContribConfiguracoesBloco0FactoryModel = class(TInterfacedObject,
    IContribConfiguracoesBloco0FactoryModel)
  private

  public
    constructor create;
    destructor Destroy; override;

    class function New: IContribConfiguracoesBloco0FactoryModel;

    function Registro0000: IContribConfiguracoesBloco0Registro0000Model;
    function Registro0001: IContribConfiguracoesBloco0Registro0001Model;
    function Registro0110: IContribConfiguracoesBloco0Registro0110Model;
    function Registro0500: IContribConfiguracoesBloco0Registro0500Model;
  end;

implementation

{ TContribConfiguracoesBloco0FactoryModel }

uses
  ContribConfiguracoesBloco0Registro0000.Model,
  ContribConfiguracoesBloco0Registro0001.Model,
  ContribConfiguracoesBloco0Registro0110.Model,
  ContribConfiguracoesBloco0Registro0500.Model;

constructor TContribConfiguracoesBloco0FactoryModel.create;
begin

end;

destructor TContribConfiguracoesBloco0FactoryModel.Destroy;
begin

  inherited;
end;

class function TContribConfiguracoesBloco0FactoryModel.New
  : IContribConfiguracoesBloco0FactoryModel;
begin
  Result := Self.create
end;

function TContribConfiguracoesBloco0FactoryModel.Registro0000
  : IContribConfiguracoesBloco0Registro0000Model;
begin
  Result := TContribConfiguracoesBloco0Registro0000Model.New;
end;

function TContribConfiguracoesBloco0FactoryModel.Registro0001
  : IContribConfiguracoesBloco0Registro0001Model;
begin
  Result := TContribConfiguracoesBloco0Registro0001Model.New;
end;

function TContribConfiguracoesBloco0FactoryModel.Registro0110
  : IContribConfiguracoesBloco0Registro0110Model;
begin
  Result := TContribConfiguracoesBloco0Registro0110Model.New;
end;

function TContribConfiguracoesBloco0FactoryModel.Registro0500
  : IContribConfiguracoesBloco0Registro0500Model;
begin
  Result := TContribConfiguracoesBloco0Registro0500Model.New;
end;

end.
