unit ContribConfiguracoesBloco0Registro0000.Model;

interface

uses
  ContribConfiguracoesBloco0Registro0000.Model.Interf,
  IniFiles,
  System.SysUtils,
  Vcl.Forms;

type
  TContribConfiguracoesBloco0Registro0000Model = class(TInterfacedObject,
    IContribConfiguracoesBloco0Registro0000Model)
  private
    FArquivo: string;
    FIniArquivo: TIniFile;

    procedure PreparaArquivoDeConfiguracoes;
    procedure ConfiguracoesPadraoRegistro0000;
  public
    constructor create;
    destructor Destroy; override;

    class function New: IContribConfiguracoesBloco0Registro0000Model;

    function PrepararConfiguracoes: IContribConfiguracoesBloco0Registro0000Model;

    function Cod_Ver: Integer;
    function Tipo_Escrit: Integer;
    function Ind_Sit_Esp: Integer;
    function Ind_Nat_PJ: Integer;
    function Ind_Ativ: Integer;
  end;

implementation

{ TContribConfiguracoesBloco0Registro0000Model }

function TContribConfiguracoesBloco0Registro0000Model.Cod_Ver: Integer;
begin
  Result := FIniArquivo.ReadInteger('Registro0000', 'Cod_Ver', 4);
end;

procedure TContribConfiguracoesBloco0Registro0000Model.ConfiguracoesPadraoRegistro0000;
begin
  FIniArquivo.WriteInteger('Registro0000', 'Cod_Ver', 4);
  FIniArquivo.WriteInteger('Registro0000', 'Tipo_Escrit', 0);
  FIniArquivo.WriteInteger('Registro0000', 'Ind_Sit_Esp', 0);
  FIniArquivo.WriteInteger('Registro0000', 'Ind_Nat_PJ', 0);
  FIniArquivo.WriteInteger('Registro0000', 'Ind_Ativ', 2);
end;

constructor TContribConfiguracoesBloco0Registro0000Model.create;
begin
  FArquivo := ExtractFileDir(Application.ExeName)+'\'+'ERGSpedContribRegistro0000.ini';
  FIniArquivo := TIniFile.create(FArquivo);
end;

destructor TContribConfiguracoesBloco0Registro0000Model.Destroy;
begin

  inherited;
end;

function TContribConfiguracoesBloco0Registro0000Model.Ind_Ativ: Integer;
begin
  Result := FIniArquivo.ReadInteger('Registro0000', 'Ind_Ativ', 2);
end;

function TContribConfiguracoesBloco0Registro0000Model.Ind_Nat_PJ: Integer;
begin
  Result := FIniArquivo.ReadInteger('Registro0000', 'Ind_Nat_PJ', 0);
end;

function TContribConfiguracoesBloco0Registro0000Model.Ind_Sit_Esp: Integer;
begin
  Result := FIniArquivo.ReadInteger('Registro0000', 'Ind_Sit_Esp', 0);
end;

class function TContribConfiguracoesBloco0Registro0000Model.New
  : IContribConfiguracoesBloco0Registro0000Model;
begin
  Result := Self.create
end;

procedure TContribConfiguracoesBloco0Registro0000Model.PreparaArquivoDeConfiguracoes;
begin
  if not(FileExists(FArquivo)) then
    ConfiguracoesPadraoRegistro0000;
end;

function TContribConfiguracoesBloco0Registro0000Model.PrepararConfiguracoes
  : IContribConfiguracoesBloco0Registro0000Model;
begin
  Result := Self;
  PreparaArquivoDeConfiguracoes;
end;

function TContribConfiguracoesBloco0Registro0000Model.Tipo_Escrit: Integer;
begin
  Result := FIniArquivo.ReadInteger('Registro0000', 'Tipo_Escrit', 0);
end;

end.
