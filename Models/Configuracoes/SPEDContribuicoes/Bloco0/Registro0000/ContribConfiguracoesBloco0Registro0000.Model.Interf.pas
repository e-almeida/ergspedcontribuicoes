unit ContribConfiguracoesBloco0Registro0000.Model.Interf;

interface

type
  IContribConfiguracoesBloco0Registro0000Model = interface
    ['{4231A052-DD76-4674-A3F5-1073F0D764DB}']
    function PrepararConfiguracoes: IContribConfiguracoesBloco0Registro0000Model;

    function Cod_Ver: Integer;
    function Tipo_Escrit: Integer;
    function Ind_Sit_Esp: Integer;
    function Ind_Nat_PJ: Integer;
    function Ind_Ativ: Integer;
  end;

implementation

end.
