unit ContribConfiguracoesBloco0Factory.Model.Interf;

interface

uses ContribConfiguracoesBloco0Registro0000.Model.Interf,
  ContribConfiguracoesBloco0Registro0001.Model.Interf,
  ContribConfiguracoesBloco0Registro0110.Model.Interf,
  ContribConfiguracoesBloco0Registro0500.Model.Interf;

type
  IContribConfiguracoesBloco0FactoryModel = interface
    function Registro0000: IContribConfiguracoesBloco0Registro0000Model;
    function Registro0001: IContribConfiguracoesBloco0Registro0001Model;
    function Registro0110: IContribConfiguracoesBloco0Registro0110Model;
    function Registro0500: IContribConfiguracoesBloco0Registro0500Model;
  end;

implementation

end.
