unit ContribConfiguracoesBloco0Registro0500.Model.Interf;

interface

type
  IContribConfiguracoesBloco0Registro0500Model = interface
    ['{8840670F-0E82-4FDE-86A9-1DDFBA63350F}']
    function PrepararConfiguracoes: IContribConfiguracoesBloco0Registro0500Model;

    function Dt_Alt: string;
    function Cod_Nat_Cc: Integer;
    function Ind_Cta: Integer;
    function Nivel: string;
    function Cod_Cta: string;
    function Nome_Cta: string;
    function Cod_Cta_Ref: string;
    function CNPJ_Est: string;
  end;

implementation

end.
