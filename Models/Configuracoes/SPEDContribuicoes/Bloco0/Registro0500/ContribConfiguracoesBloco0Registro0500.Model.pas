unit ContribConfiguracoesBloco0Registro0500.Model;

interface

uses
  ContribConfiguracoesBloco0Registro0500.Model.Interf,
  Inifiles, System.SysUtils, vcl.forms;

type
  TContribConfiguracoesBloco0Registro0500Model = class(TInterfacedObject,
    IContribConfiguracoesBloco0Registro0500Model)
  private
    FArquivo: string;
    FIniArquivo: TIniFile;

    procedure PreparaArquivoDeConfiguracoes;
    procedure ConfiguracoesPadraoRegistro0500;
  public
    constructor create;
    destructor Destroy; override;

    class function New: IContribConfiguracoesBloco0Registro0500Model;

    function PrepararConfiguracoes: IContribConfiguracoesBloco0Registro0500Model;

    function Dt_Alt: string;
    function Cod_Nat_Cc: Integer;
    function Ind_Cta: Integer;
    function Nivel: string;
    function Cod_Cta: string;
    function Nome_Cta: string;
    function Cod_Cta_Ref: string;
    function CNPJ_Est: string;
  end;

implementation

{ TContribConfiguracoesBloco0Registro0500Model }

function TContribConfiguracoesBloco0Registro0500Model.CNPJ_Est: string;
begin
  Result := FIniArquivo.ReadString('Registro0500', 'CNPJ_Est', '');
end;

function TContribConfiguracoesBloco0Registro0500Model.Cod_Cta: string;
begin
  Result := FIniArquivo.ReadString('Registro0500', 'Cod_Cta', '');
end;

function TContribConfiguracoesBloco0Registro0500Model.Cod_Cta_Ref: string;
begin
  Result := FIniArquivo.ReadString('Registro0500', 'Cod_Cta_Ref', '');
end;

function TContribConfiguracoesBloco0Registro0500Model.Cod_Nat_Cc: Integer;
begin
  Result := FIniArquivo.ReadInteger('Registro0500', 'Cod_Nat_Cc', 0);
end;

procedure TContribConfiguracoesBloco0Registro0500Model.ConfiguracoesPadraoRegistro0500;
begin
  FIniArquivo.WriteString('Registro0500',  'Dt_Alt',      FormatDateTime('DD/MM/YYYY', Now));
  FIniArquivo.WriteInteger('Registro0500', 'Cod_Nat_Cc',  0);
  FIniArquivo.WriteInteger('Registro0500', 'Ind_Cta',     0);
  FIniArquivo.WriteString('Registro0500',  'Nivel',       '0');
  FIniArquivo.WriteString('Registro0500',  'Cod_Cta',     '01');
  FIniArquivo.WriteString('Registro0500',  'Nome_Cta',    'NOME DA CONTA');
  FIniArquivo.WriteString('Registro0500',  'Cod_Cta_Ref', '00100');
  FIniArquivo.WriteString('Registro0500',  'CNPJ_Est',    '33333333000191');
end;

constructor TContribConfiguracoesBloco0Registro0500Model.create;
begin
  FArquivo    := ExtractFileDir(Application.ExeName) + '\' +'ERGSpedContribRegistro0500.ini';
  FIniArquivo := TIniFile.create(FArquivo);
end;

destructor TContribConfiguracoesBloco0Registro0500Model.Destroy;
begin

  inherited;
end;

function TContribConfiguracoesBloco0Registro0500Model.Dt_Alt: string;
begin
  Result := FIniArquivo.ReadString('Registro0500', 'Dt_Alt', '');
end;

function TContribConfiguracoesBloco0Registro0500Model.Ind_Cta: Integer;
begin
  Result := FIniArquivo.ReadInteger('Registro0500', 'Ind_Cta', 0);
end;

class function TContribConfiguracoesBloco0Registro0500Model.New
  : IContribConfiguracoesBloco0Registro0500Model;
begin
  Result := Self.create
end;

function TContribConfiguracoesBloco0Registro0500Model.Nivel: string;
begin
  Result := FIniArquivo.ReadString('Registro0500', 'Nivel', '');
end;

function TContribConfiguracoesBloco0Registro0500Model.Nome_Cta: string;
begin
  Result := FIniArquivo.ReadString('Registro0500', 'Nome_Cta', '');
end;

procedure TContribConfiguracoesBloco0Registro0500Model.PreparaArquivoDeConfiguracoes;
begin
  if Not(FileExists(FArquivo)) then
  ConfiguracoesPadraoRegistro0500;
end;

function TContribConfiguracoesBloco0Registro0500Model.PrepararConfiguracoes
  : IContribConfiguracoesBloco0Registro0500Model;
begin
  Result := Self;
  PreparaArquivoDeConfiguracoes;
end;

end.
