unit ContribConfiguracoesFacade.Model;

interface

uses ContribConfiguracoesFacade.Model.Interf,
  ContribConfiguracoesBloco0Factory.Model.Interf;

type
  TContribConfiguracoesFacadeModel = class(TInterfacedObject,
    IContribConfiguracoesFacadeModel)
  private

  public
    constructor create;
    destructor Destroy; override;

    class function New: IContribConfiguracoesFacadeModel;

    function Bloco0: IContribConfiguracoesBloco0FactoryModel;
  end;

implementation

{ TContribConfiguracoesFacadeModel }

uses ContribConfiguracoesBloco0Factory.Model;

function TContribConfiguracoesFacadeModel.Bloco0
  : IContribConfiguracoesBloco0FactoryModel;
begin
  Result := TContribConfiguracoesBloco0FactoryModel.New;
end;

constructor TContribConfiguracoesFacadeModel.create;
begin

end;

destructor TContribConfiguracoesFacadeModel.Destroy;
begin

  inherited;
end;

class function TContribConfiguracoesFacadeModel.New
  : IContribConfiguracoesFacadeModel;
begin
  Result := Self.create
end;

end.
