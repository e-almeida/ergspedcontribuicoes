unit ContribConfiguracoesFacade.Model.Interf;

interface

uses ContribConfiguracoesBloco0Factory.Model.Interf;

type
  IContribConfiguracoesFacadeModel = interface
    ['{F6D25547-D418-4696-9F08-F925F1E61FF1}']
    function Bloco0: IContribConfiguracoesBloco0FactoryModel;
  end;

implementation

end.
