unit ConfiguracoesConexao.Model.Interf;

interface

type
  IConfiguracoesConexaoModel = interface
    ['{CE6A4915-B76E-4D9C-90DB-E32DE55D1CF8}']
    function PrepararConfiguracoes: IConfiguracoesConexaoModel;

    function BancoDeDados: string;
    function Driver: string;
    function Porta: string;
    function Servidor: string;
    function Usuario: string;
    function Senha: string;
    function Biblioteca: string;
  end;

implementation

end.
