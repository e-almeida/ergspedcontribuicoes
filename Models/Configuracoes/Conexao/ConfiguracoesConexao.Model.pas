unit ConfiguracoesConexao.Model;

interface

uses
  ConfiguracoesConexao.Model.Interf,
  IniFiles,
  System.SysUtils,
  Vcl.Forms;

type
  TConfiguracoesModel = class(TInterfacedObject, IConfiguracoesConexaoModel)
  private
    FArquivo: string;
    FIniArquivo: TIniFile;

    procedure PreparaArquivoDeConfiguracoes;
    procedure ConfiguracoesDeConexaoBancoDeDados;
  public
    constructor create;
    destructor Destroy; override;

    class function New: IConfiguracoesConexaoModel;

    function PrepararConfiguracoes: IConfiguracoesConexaoModel;

    function BancoDeDados: string;
    function Driver: string;
    function Porta: string;
    function Servidor: string;
    function Usuario: string;
    function Senha: string;
    function Biblioteca: string;
  end;

implementation

{ TConfiguracoesModel }

procedure TConfiguracoesModel.PreparaArquivoDeConfiguracoes;
begin
  if Not(FileExists(FArquivo)) then
   {1} ConfiguracoesDeConexaoBancoDeDados;
end;

function TConfiguracoesModel.BancoDeDados: string;
begin
   Result := FIniArquivo.ReadString('Dados','BancoDeDados','');
end;

function TConfiguracoesModel.Biblioteca: string;
begin
   Result := FIniArquivo.ReadString('Dados','Biblioteca','');
end;

procedure TConfiguracoesModel.ConfiguracoesDeConexaoBancoDeDados;
begin
  FIniArquivo.WriteString('Dados', 'BancoDeDados', 'C:\ergsis\dados\ergdados.erg');
  FIniArquivo.WriteString('Dados', 'Driver',       'FB');
  FIniArquivo.WriteString('Dados', 'Porta',        '3050');
  FIniArquivo.WriteString('Dados', 'Servidor',     'LocalHost');
  FIniArquivo.WriteString('Dados', 'Usuario',      'SYSDBA');
  FIniArquivo.WriteString('Dados', 'Senha',        'masterkey');
  FIniArquivo.WriteString('Dados', 'Biblioteca',   'FbClient.dll');
end;

constructor TConfiguracoesModel.create;
begin
  FArquivo    := ChangeFileExt(Application.ExeName, '.ini');
  FIniArquivo := TIniFile.create(FArquivo);
end;

destructor TConfiguracoesModel.Destroy;
begin

  inherited;
end;

function TConfiguracoesModel.Driver: string;
begin
   Result := FIniArquivo.ReadString('Dados','Driver','');
end;

class function TConfiguracoesModel.New: IConfiguracoesConexaoModel;
begin
  Result := Self.create
end;

function TConfiguracoesModel.Porta: string;
begin
   Result := FIniArquivo.ReadString('Dados','Porta','');
end;

function TConfiguracoesModel.PrepararConfiguracoes: IConfiguracoesConexaoModel;
begin
  Result := Self;
   {1} PreparaArquivoDeConfiguracoes;
end;

function TConfiguracoesModel.Senha: string;
begin
   Result := FIniArquivo.ReadString('Dados','Senha','');
end;

function TConfiguracoesModel.Servidor: string;
begin
   Result := FIniArquivo.ReadString('Dados','Servidor','');
end;

function TConfiguracoesModel.Usuario: string;
begin
   Result := FIniArquivo.ReadString('Dados','Usuario','');
end;

end.
