unit ConfiguracoesFactory.Model;

interface

uses ConfiguracoesFactory.Model.Interf, ConfiguracoesConexao.Model.Interf,
  ContribConfiguracoesFacade.Model.Interf;

type
  TConfiguracoesFactoryModel = class(TInterfacedObject,
    IConfiguracoesFactoryModel)
  private

  public
    constructor create;
    destructor Destroy; override;

    class function New: IConfiguracoesFactoryModel;

    function Conexao: IConfiguracoesConexaoModel;
    function SPEDContribuicoes: IContribConfiguracoesFacadeModel;
  end;

implementation

{ TConfiguracoesFactoryModel }

uses ConfiguracoesConexao.Model, ContribConfiguracoesFacade.Model;

function TConfiguracoesFactoryModel.Conexao: IConfiguracoesConexaoModel;
begin
  Result := TConfiguracoesModel.New;
end;

constructor TConfiguracoesFactoryModel.create;
begin

end;

destructor TConfiguracoesFactoryModel.Destroy;
begin

  inherited;
end;

class function TConfiguracoesFactoryModel.New: IConfiguracoesFactoryModel;
begin
  Result := Self.create
end;

function TConfiguracoesFactoryModel.SPEDContribuicoes
  : IContribConfiguracoesFacadeModel;
begin
  Result := TContribConfiguracoesFacadeModel.New;
end;

end.
