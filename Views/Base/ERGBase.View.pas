unit ERGBase.View;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, RzLabel, Vcl.ExtCtrls,
  RzPanel;

type
  TFERGBaseView = class(TForm)
    PnLayout: TRzPanel;
    PnMenuInferior: TRzPanel;
    PnPrograma: TRzPanel;
    LBPrograma: TRzLabel;
    PnConfirmar: TRzPanel;
    PnGrid: TRzPanel;
    PnGrade: TRzPanel;
    procedure PnConfirmarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FERGBaseView: TFERGBaseView;

implementation

{$R *.dfm}

procedure TFERGBaseView.PnConfirmarClick(Sender: TObject);
begin
  Close;
end;

end.
