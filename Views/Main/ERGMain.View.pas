unit ERGMain.View;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, RzLabel, Vcl.ExtCtrls,
  RzPanel, ACBrBase, ACBrSpedPisCofins, RzEdit, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, cxButtons, Vcl.Mask,
  Vcl.ComCtrls, AdvDateTimePicker, ERGDateTime, cxControls, cxContainer, cxEdit,
  dxCore, cxDateUtils, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  AdvEdit, ERGEdit, cxMemo, dxGDIPlusClasses;

type
  TFERGMainView = class(TForm)
    PnLayout       : TRzPanel;
    PnMenuInferior : TRzPanel;
    PnSair         : TRzPanel;
    PnPrograma     : TRzPanel;
    LBPrograma     : TRzLabel;
    PnConfirmar    : TRzPanel;
    ACBrSPED       : TACBrSPEDPisCofins;
    PnGrid         : TRzPanel;
    PnGrade        : TRzPanel;
    PnDetalhes     : TRzPanel;
    PnEnviarEmail  : TRzPanel;
    Image1         : TImage;
    EBSPEDLog      : TcxMemo;
    procedure PnSairClick(Sender: TObject);
    procedure PnConfirmarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FERGMainView: TFERGMainView;

implementation

{$R *.dfm}

uses ERGSped.Controller;

procedure TFERGMainView.FormCreate(Sender: TObject);
begin
  LBPrograma.Caption := Self.Name;
end;

procedure TFERGMainView.PnConfirmarClick(Sender: TObject);
begin
  TERGSpedController
   .New
    .SPED
     .Contribuicoes
      .ACBrSPEDPisCofins(ACBrSPED)
       .EmpresaSelecionada(1)
       .DataInicial(StrToDate('01/09/2018'))
       .DataFinal(StrToDate('30/09/2018'))
       .DiretorioArquivo('c:\ello\SPED\')
     .Iniciar
      .Bloco0
      .BlocoA
      .BlocoC
      .BlocoD
      .BlocoF
      .BlocoM
      .Bloco1
      .BlocoP
    .&End;

   EBSPEDLog.Lines.LoadFromFile('C:\Ello\SPED\SPEDContrib_01_09_2018_�_30_09_2018.txt');
end;

procedure TFERGMainView.PnSairClick(Sender: TObject);
begin
  Close;
end;

end.
