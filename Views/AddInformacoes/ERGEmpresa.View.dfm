inherited FERGEmpresaView: TFERGEmpresaView
  Caption = 'Empresa'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PnLayout: TRzPanel
    inherited PnGrid: TRzPanel
      inherited PnGrade: TRzPanel
        object CBEmpresa: TERGComboBox
          Left = 63
          Top = 15
          Width = 354
          Height = 21
          Color = clWindow
          Version = '1.6.1.1'
          Visible = True
          BorderColor = clSilver
          EmptyTextStyle = []
          DropWidth = 0
          Enabled = True
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ItemIndex = -1
          LabelCaption = 'Empresa:'
          LabelMargin = 10
          LabelFont.Charset = ANSI_CHARSET
          LabelFont.Color = clWindowText
          LabelFont.Height = -11
          LabelFont.Name = 'MS Sans Serif'
          LabelFont.Style = []
          ParentFont = False
          TabOrder = 0
          Required = False
          RequiredBorderColor = clBlack
          RequiredColor = clBlack
          RequiredFontColor = clBlack
        end
      end
    end
  end
end
