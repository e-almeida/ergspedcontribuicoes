unit ERGEmpresa.View;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ERGBase.View, Vcl.StdCtrls, RzLabel,
  Vcl.ExtCtrls, RzPanel, AdvCombo, ERGComboBox;

type
  TFERGEmpresaView = class(TFERGBaseView)
    CBEmpresa: TERGComboBox;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FERGEmpresaView: TFERGEmpresaView;

implementation

{$R *.dfm}

end.
