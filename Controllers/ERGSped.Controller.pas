unit ERGSped.Controller;

interface

uses ERGSped.Controller.interf, SPEDFactory.Controller.interf;

type
  TERGSpedController = class(TInterfacedObject, IERGSpedController)
  private

  public
    constructor create;
    destructor Destroy; override;

    class function New: IERGSpedController;

    function SPED: ISPEDFactoryController;

  end;

implementation

{ TERGSpedController }

uses SPEDFactory.Controller;

constructor TERGSpedController.create;
begin

end;

destructor TERGSpedController.Destroy;
begin

  inherited;
end;

class function TERGSpedController.New: IERGSpedController;
begin
  Result := Self.create
end;

function TERGSpedController.SPED: ISPEDFactoryController;
begin
  Result := TSPEDFactoryController.New;
end;

end.
