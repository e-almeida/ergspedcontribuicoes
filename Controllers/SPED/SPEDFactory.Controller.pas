unit SPEDFactory.Controller;

interface

uses SPEDContribuicoes.Controller.interf, SPEDFactory.Controller.interf;

type
  TSPEDFactoryController = class(TInterfacedObject, ISPEDFactoryController)
  private

  public
    constructor create;
    destructor Destroy; override;

    class function New: ISPEDFactoryController;

    function Contribuicoes: ISPEDContribuicoesController;
  end;

implementation

{ TSPEDFactoryController }

uses SPEDContribuicoes.Controller;

function TSPEDFactoryController.Contribuicoes: ISPEDContribuicoesController;
begin
  Result := TSPEDContribuicoesController.New;
end;

constructor TSPEDFactoryController.create;
begin

end;

destructor TSPEDFactoryController.Destroy;
begin

  inherited;
end;

class function TSPEDFactoryController.New: ISPEDFactoryController;
begin
  Result := Self.create
end;

end.
