unit SPEDFactory.Controller.interf;

interface

uses SPEDContribuicoes.Controller.interf;

type
  ISPEDFactoryController = interface
    ['{FCF27A84-BD6A-48C8-A9C5-56CC1C935E78}']
    function Contribuicoes: ISPEDContribuicoesController;
  end;

implementation

end.
