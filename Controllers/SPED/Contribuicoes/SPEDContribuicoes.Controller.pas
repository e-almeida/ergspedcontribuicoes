unit SPEDContribuicoes.Controller;

interface

uses
  SPEDContribuicoes.Controller.interf,
  ACBrEPCBlocos,
  ACBrSpedPisCofins,
  ACBrUtil,
  ACBrTXTClass,
  ACBrBase,
  SPEDContribuicoesBloco0.Model.Interf,
  System.SysUtils, SPEDContribuicoesBlocoA.Model.Interf;

type
  TSPEDContribuicoesController = class(TInterfacedObject,
    ISPEDContribuicoesController)
  private
    FSPEDPisCofins    : TACBrSPEDPisCofins;
    FBloco0           : ISPEDContribuicoesBloco0Model;
    FBlocoA           : ISPEDContribuicoesBlocoAModel;
    FEmpresa          : Integer;
    FDataInicial      : TDate;
    FDataFinal        : TDate;
    FDiretorioArquivo : string;
    FArquivo          : string;
  public
    constructor create;
    destructor Destroy; override;

    class function New: ISPEDContribuicoesController;

    function ACBrSPEDPisCofins(AValue: TACBrSPEDPisCofins): ISPEDContribuicoesController;
    function EmpresaSelecionada(AValue: Integer): ISPEDContribuicoesController;
    function DataInicial(AValue: TDate): ISPEDContribuicoesController;
    function DataFinal(AValue: TDate): ISPEDContribuicoesController;
    function DiretorioArquivo(AValue: string): ISPEDContribuicoesController;

    function Iniciar:ISPEDContribuicoesController;

    function Bloco0: ISPEDContribuicoesController;
    function BlocoA: ISPEDContribuicoesController;
    function BlocoC: ISPEDContribuicoesController;
    function BlocoD: ISPEDContribuicoesController;
    function BlocoF: ISPEDContribuicoesController;
    function BlocoM: ISPEDContribuicoesController;
    function Bloco1: ISPEDContribuicoesController;
    function BlocoP: ISPEDContribuicoesController;

    Function &End: ISPEDContribuicoesController;
  end;

implementation

{ TSPEDContribuicoesController }

uses

  ACBrEPCBloco_1,
  ACBrEPCBloco_A,
  ACBrEPCBloco_C,
  ACBrEPCBloco_D,
  ACBrEPCBloco_F,
  ACBrEPCBloco_M,
  ERGSped.Model;

function TSPEDContribuicoesController.ACBrSPEDPisCofins(
  AValue: TACBrSPEDPisCofins): ISPEDContribuicoesController;
begin
  Result := Self;
  FSPEDPisCofins := AValue;
end;

function TSPEDContribuicoesController.Bloco0: ISPEDContribuicoesController;
begin
  Result := Self;

  with FSPEDPisCofins do
  begin
      Bloco_0 := FBloco0
                   .Empresa(FEmpresa)
                    .DataInicial(FDataInicial)
                     .DataFinal(FDataFinal)
                      .Registro0000
                      .Registro0001
                      .Registro0100
                      .Registro0110
                      .Registro0140
                      .Registro0145
                      .Registro0150
                      .Registro0190
                      .Registro0200
                      .Registro0205
                      .Registro0500
                     .&End;
   end;
end;

function TSPEDContribuicoesController.Bloco1: ISPEDContribuicoesController;
begin
  Result := Self;
end;

function TSPEDContribuicoesController.BlocoA: ISPEDContribuicoesController;
begin
  Result := Self;

  with FSPEDPisCofins do
  begin
    Bloco_A := FBlocoA
                .Empresa(FEmpresa)
                 .DataInicial(FDataInicial)
                  .DataFinal(FDataFinal)
                   .RegistroA001
                   .RegistroA010
                   .RegistroA100
                   .RegistroA170
                  .&End;
  end;
end;

function TSPEDContribuicoesController.BlocoC: ISPEDContribuicoesController;
begin
  Result := Self;
end;

function TSPEDContribuicoesController.BlocoD: ISPEDContribuicoesController;
begin
  Result := Self;
end;

function TSPEDContribuicoesController.BlocoF: ISPEDContribuicoesController;
begin
  Result := Self;
end;

function TSPEDContribuicoesController.BlocoM: ISPEDContribuicoesController;
begin
  Result := Self;
end;

function TSPEDContribuicoesController.BlocoP: ISPEDContribuicoesController;
begin
  Result := Self;
end;

function TSPEDContribuicoesController.&End: ISPEDContribuicoesController;
begin
  Result := Self;

  FSPEDPisCofins.SaveFileTXT;
end;

function TSPEDContribuicoesController.Iniciar: ISPEDContribuicoesController;
begin
  Result := Self;

  FSPEDPisCofins.DT_INI  := FDataInicial;
  FSPEDPisCofins.DT_FIN  := FDataFinal;
  FSPEDPisCofins.Path    := FDiretorioArquivo;

  FArquivo := 'SPEDContrib_'+FormatDateTime('DD_MM_YYYY', FDataInicial)+'_�_'
                            +FormatDateTime('DD_MM_YYYY', FDataFinal)+'.txt';

  FSPEDPisCofins.Arquivo := FArquivo;
end;

constructor TSPEDContribuicoesController.create;
begin
  FBloco0 := TERGSPEDModel.New.SPED.Contribuicoes.Bloco0;
  FBlocoA := TERGSPEDModel.New.SPED.Contribuicoes.BlocoA;
end;

function TSPEDContribuicoesController.DataFinal(
  AValue: TDate): ISPEDContribuicoesController;
begin
  Result := Self;
  FDataFinal := AValue;
end;

function TSPEDContribuicoesController.DataInicial(
  AValue: TDate): ISPEDContribuicoesController;
begin
  Result := Self;
  FDataInicial := AValue;
end;

destructor TSPEDContribuicoesController.Destroy;
begin

  inherited;
end;

function TSPEDContribuicoesController.DiretorioArquivo(
  AValue: string): ISPEDContribuicoesController;
begin
  Result := Self;
  FDiretorioArquivo   := AValue;
end;

function TSPEDContribuicoesController.EmpresaSelecionada(
  AValue: Integer): ISPEDContribuicoesController;
begin
  Result := Self;
  FEmpresa := AValue;
end;

class function TSPEDContribuicoesController.New: ISPEDContribuicoesController;
begin
  Result := Self.create
end;

end.
