unit SPEDContribuicoes.Controller.interf;

interface

uses
  ACBrEPCBlocos,
  ACBrSpedPisCofins,
  ACBrUtil,
  ACBrTXTClass,
  ACBrBase;

type
  ISPEDContribuicoesController = interface
    ['{0CD8126B-744F-4A78-80C3-2F9F23A2740A}']
    function ACBrSPEDPisCofins(AValue: TACBrSPEDPisCofins): ISPEDContribuicoesController;
    function EmpresaSelecionada(AValue: Integer): ISPEDContribuicoesController;
    function DataInicial(AValue: TDate): ISPEDContribuicoesController;
    function DataFinal(AValue: TDate): ISPEDContribuicoesController;
    function DiretorioArquivo(AValue: string): ISPEDContribuicoesController;
    function Iniciar:ISPEDContribuicoesController;
    function Bloco0: ISPEDContribuicoesController;
    function BlocoA: ISPEDContribuicoesController;
    function BlocoC: ISPEDContribuicoesController;
    function BlocoD: ISPEDContribuicoesController;
    function BlocoF: ISPEDContribuicoesController;
    function BlocoM: ISPEDContribuicoesController;
    function Bloco1: ISPEDContribuicoesController;
    function BlocoP: ISPEDContribuicoesController;
    Function &End: ISPEDContribuicoesController;
  end;

implementation

end.
