unit ERGSped.Controller.interf;

interface

uses SPEDFactory.Controller.interf;



type
 IERGSpedController = interface
   ['{80CFA3E6-C9B3-41B0-B050-54499B033389}']
   function SPED: ISPEDFactoryController;
 end;

implementation

end.
